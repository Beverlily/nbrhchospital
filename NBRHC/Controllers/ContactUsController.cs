﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;
using System.Net;
using System.Web;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using NBRHC.Models;
using NBRHC.Data;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System.Diagnostics;


namespace NBRHC.Controllers
{
    //Note: This controller references Christine Bittle's http5204-blog-aspcore example.
    //Comments as well as code may look similar for learning purposes but is applied to this specific feature
    public class ContactUsController : Controller
    {
        //Make a Db Context for CRUD
        private readonly NBRHCCMSContext db;
        //We need the usermanager class to get things like the id or name
        private readonly UserManager<ApplicationUser> _userManager;

        //return current user 
        private async Task<ApplicationUser> GetCurrentUserAsync() => await _userManager.GetUserAsync(HttpContext.User);
        public ContactUsController(NBRHCCMSContext context, UserManager<ApplicationUser> usermanager)
        {
            //When our ContactUs controller is created we need 2 things
            //Db Context for CRUD
            db = context;
            //User manager for getting the current logged in user
            _userManager = usermanager;
        }
        public async Task<int> GetUserDetails(ApplicationUser user)
        {
            //UserState
            //0 => Public user 
            //1 => User but has no admin account 
            //2 => User has admin account 
            if (user == null) return 0;
            if (user.AdminID == null) return 1;
            else
            {
                return 2;
            }
        }

        // GET: ContactUs
        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        public async Task<ActionResult> List(int pagenum)
        {
            //Get current user
            var user = await GetCurrentUserAsync();
            var userstate = await GetUserDetails(user);

            //Info on user
            ViewData["UserState"] = userstate;
            ViewData["UserAdminID"] = 0;

            //Pagination
            //Referenced from:
            //Christine Bittle's http5204-blog-aspcore example BlogController.cs line 135
            var _messages = await db.ContactUs.ToListAsync();
            int messagecount = _messages.Count();
            int perpage = 5;
            int maxpage = (int)Math.Ceiling((decimal)messagecount / perpage) - 1;
            if (maxpage < 0) maxpage = 0;
            if (pagenum < 0) pagenum = 0;
            if (pagenum > maxpage) pagenum = maxpage;
            int start = perpage * pagenum;
            ViewData["pagenum"] = (int)pagenum;
            ViewData["PaginationSummary"] = "";
            if(maxpage > 0)
            {
                ViewData["PaginationSummary"] =
                    (pagenum + 1).ToString() + " of " +
                    (maxpage + 1).ToString();
            }
            
            if(userstate == 2)
            {
                ViewData["UserAdminID"] = user.AdminID;
            }

            return View(await db.ContactUs.Skip(start).Take(perpage).ToListAsync());
        }

        // GET: ContactUs/Details
        public async Task<ActionResult> Details(int? id)
        {
            //Get our current user
            var user = await GetCurrentUserAsync();
            var userstate = await GetUserDetails(user);
            ViewData["UserState"] = userstate;
            
            if (userstate == 0)
            {
                return new StatusCodeResult(400);
            }
            
            if(userstate == 2)
            {
                ViewData["UserAdminID"] = user.AdminID;
            }

            return View(await db.ContactUs.SingleOrDefaultAsync(c => c.ContactID == id));
        }

        // GET: ContactUs/Create
        public ActionResult New()
        {
            return View();
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult New([Bind("ContactID, ContactName, ContactEmail, ContactPhone, ContactSubject, ContactMessage")] ContactUs contact)
        {
            if(ModelState.IsValid)
            {
                db.ContactUs.Add(contact);
                db.SaveChanges();
                TempData["AddSuccess"] = "Message successfully sent";
                return RedirectToAction("Index");
            }
            else
            {
                TempData["AddFail"] = "Failed to send message";
                return RedirectToAction("Index");
            }
        }

        // ContactUs/Delete
        public async Task<ActionResult> Delete(int? id)
        {
            ContactUs contactus = db.ContactUs.Find(id);
            if (id == null || contactus == null)
            {
                return NotFound();
            }
            else
            {
                var user = await GetCurrentUserAsync();
                var userstate = await GetUserDetails(user);

                if (userstate == 0 || userstate == 1)
                {
                    return Forbid();
                }
                else
                {
                    db.ContactUs.Remove(contactus);
                    db.SaveChanges();
                    TempData["DeleteSuccess"] = "Message successfully deleted";
                    return RedirectToAction("Index");
                }
            }
        }



    }
}