﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.FileProviders;
using NBRHC.Models;

namespace NBRHC.Controllers
{
    public class EventAdminController : Controller
    {
        private readonly IFileProvider fileProvider;

        public EventAdminController(IFileProvider fileProvider)
        {
            this.fileProvider = fileProvider;
        }

        string connString = ConnString.connString;
        // GET: EventAdmin
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult UpdateEvent()
        {
            DataTable dtbl = new DataTable();
            using (SqlConnection sqlcon = new SqlConnection(connString))
            {
                sqlcon.Open();
                SqlDataAdapter sqlda = new SqlDataAdapter("SELECT * FROM Event_detail", sqlcon);
                sqlda.Fill(dtbl);
            }
            return View(dtbl);
        }

        // GET: EventAdmin/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: EventAdmin/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(EventAdminModel eam)
        {
            if (eam == null ||
                eam.FileToUpload == null || eam.FileToUpload.Length == 0)
                return Content("file not selected");

            var path = Path.Combine(
                        Directory.GetCurrentDirectory(), "wwwroot",
                        eam.FileToUpload.GetFilename());

            using (var stream = new FileStream(path, FileMode.Create))
            {
                await eam.FileToUpload.CopyToAsync(stream);
            }

            using (SqlConnection sqlcon = new SqlConnection(connString))
            {
                sqlcon.Open();
                string quary = "INSERT INTO Event_detail VALUES(@Title,@Des,@stime,@etime,@sdate,@edate,@location,@image)";
                SqlCommand sqlcmd = new SqlCommand(quary, sqlcon);
                sqlcmd.Parameters.AddWithValue("@Title", eam.Title);
                sqlcmd.Parameters.AddWithValue("@Des", eam.Description);
                sqlcmd.Parameters.AddWithValue("@stime", eam.stime);
                sqlcmd.Parameters.AddWithValue("@etime", eam.etime);
                DateTime sdateTime = Convert.ToDateTime(eam.sdate).Date;
                string dateonly = sdateTime.ToString("MM-dd-yyyy");
                sqlcmd.Parameters.AddWithValue("@sdate", dateonly);
                DateTime edateTime = Convert.ToDateTime(eam.edate).Date;
                string dateonly1 = edateTime.ToString("MM-dd-yyyy");
                sqlcmd.Parameters.AddWithValue("@edate", dateonly1);
                sqlcmd.Parameters.AddWithValue("@location", eam.Location);
                sqlcmd.Parameters.AddWithValue("@image", eam.FileToUpload.FileName);
                sqlcmd.ExecuteNonQuery();
            }
            return RedirectToAction("Create");
        }

        // GET: EventAdmin/Edit/5
        public ActionResult Edit(int id)
        {
            EventAdminModel eam = new EventAdminModel();
            DataTable datatable = new DataTable();
            using (SqlConnection sqlcon = new SqlConnection(connString))
            {
                sqlcon.Open();
                SqlDataAdapter sqlda = new SqlDataAdapter("SELECT * FROM Event_detail where event_Id = @Id", sqlcon);
                sqlda.SelectCommand.Parameters.AddWithValue("@Id", id);
                sqlda.Fill(datatable);
            }
            if (datatable.Rows.Count == 1)
            {
                eam.Id = Convert.ToInt32(datatable.Rows[0][0].ToString());
                eam.Title = datatable.Rows[0][1].ToString();
                eam.Description = datatable.Rows[0][2].ToString();
                eam.stime = datatable.Rows[0][3].ToString();
                eam.etime = datatable.Rows[0][4].ToString();
                eam.sdate = datatable.Rows[0][5].ToString();
                eam.edate = datatable.Rows[0][6].ToString();
                eam.Location = datatable.Rows[0][7].ToString();
                eam.Image = datatable.Rows[0][8].ToString();
                return View(eam);
            }
            else
                return RedirectToAction("UpdateEvent");
        }

        // POST: EventAdmin/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(EventAdminModel eam)
        {
            if (eam == null ||
                eam.FileToUpload == null || eam.FileToUpload.Length == 0)
                return Content("file not selected");

            var path = Path.Combine(
                        Directory.GetCurrentDirectory(), "wwwroot",
                        eam.FileToUpload.GetFilename());

            using (var stream = new FileStream(path, FileMode.Create))
            {
                await eam.FileToUpload.CopyToAsync(stream);
            }
            using (SqlConnection sqlcon = new SqlConnection(connString))
            {
                sqlcon.Open();
                string quary = "Update Event_detail SET event_title=@Title , description=@Des , start_time=@stime , end_time=@etime , start_date=@sdate , end_date=@edate , location=@location , image=@image WHERE event_Id=@Id ";
                SqlCommand sqlcmd = new SqlCommand(quary, sqlcon);
                sqlcmd.Parameters.AddWithValue("@Id", eam.Id);
                sqlcmd.Parameters.AddWithValue("@Title", eam.Title);
                sqlcmd.Parameters.AddWithValue("@Des", eam.Description);
                sqlcmd.Parameters.AddWithValue("@stime", eam.stime);
                sqlcmd.Parameters.AddWithValue("@etime", eam.etime);
                DateTime sdateTime = Convert.ToDateTime(eam.sdate).Date;
                string dateonly = sdateTime.ToString("MM-dd-yyyy");
                sqlcmd.Parameters.AddWithValue("@sdate", dateonly);
                DateTime edateTime = Convert.ToDateTime(eam.edate).Date;
                string dateonly1 = edateTime.ToString("MM-dd-yyyy");
                sqlcmd.Parameters.AddWithValue("@edate", dateonly1);
                sqlcmd.Parameters.AddWithValue("@location", eam.Location);
                sqlcmd.Parameters.AddWithValue("@image", eam.FileToUpload.FileName);
                sqlcmd.ExecuteNonQuery();
            }
            return RedirectToAction("UpdateEvent");
        }

        // GET: EventAdmin/Delete/5
        public ActionResult Delete(int id)
        {
            using (SqlConnection sqlcon = new SqlConnection(connString))
            {
                sqlcon.Open();
                string quary = "DELETE FROM Event_detail WHERE event_Id=@Id ";
                SqlCommand sqlcmd = new SqlCommand(quary, sqlcon);
                sqlcmd.Parameters.AddWithValue("@Id", id);
                sqlcmd.ExecuteNonQuery();
            }
            return RedirectToAction("UpdateEvent");
        }

        // POST: EventAdmin/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}