﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;
using System.Net;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using NBRHC.Models;
using NBRHC.Models.ViewModels;
using NBRHC.Data;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System.Diagnostics;

namespace NBRHC.Controllers
{
    public class VolunteerController : Controller
    {
        private NBRHCCMSContext db;
        private readonly IHostingEnvironment _env;
        private readonly UserManager<ApplicationUser> _userManager;

        public VolunteerController(NBRHCCMSContext context)
        {
            db = context;
        }

        //Methods:
        //Show
        //New
        //List
        //Create

        public ActionResult Show(int id)
        {
            //Return a view because we're just going to the 'Show' page
            var volunteer = db.Volunteers.Where(d => d.id == id).FirstOrDefault();
            return View(volunteer);
        }

        public ActionResult New()
        {
            //Return a view because we're just going to the 'new' page
            return View();
        }
        
        public ActionResult Create(string formfirstname, string formlastname, string formcellphone, string formhomephone,
            string formcity, string formstreetnumber, string formemail)
        {
            Debug.WriteLine("First name: " + formfirstname);
            Debug.WriteLine("Last name: " + formlastname);
            Debug.WriteLine("Cell phone: " + formcellphone);
            Debug.WriteLine("Home phone: " + formhomephone);
            Debug.WriteLine("City: " + formcity);
            Debug.WriteLine("Street number: " + formstreetnumber);
            Debug.WriteLine("Email: " + formemail);

            var query = "INSERT INTO volunteers (firstName, lastName, cellPhoneNo, homePhoneNo, city, streetNumber, email)";
            query += "VALUES (@first, @last, @cell, @home, @city, @street, @email)";

            SqlParameter[] sqlparams = new SqlParameter[7];
            sqlparams[0] = new SqlParameter("@first", formfirstname);
            sqlparams[1] = new SqlParameter("@last", formlastname);
            sqlparams[2] = new SqlParameter("@cell", formcellphone);
            sqlparams[3] = new SqlParameter("@home", formhomephone);
            sqlparams[4] = new SqlParameter("@city", formcity);
            sqlparams[5] = new SqlParameter("@street", formstreetnumber);
            sqlparams[6] = new SqlParameter("@email", formemail);

            db.Database.ExecuteSqlCommand(query, sqlparams);

            return RedirectToAction("list");
        }

        public ActionResult List()
        {
            //We want to give the view a list of volunteers.
            var volunteers = db.Volunteers.ToList();
            return View(volunteers);
        }

        public ActionResult Delete(int? id)
        {
            string query = "DELETE FROM Volunteers WHERE id = @id";
            SqlParameter param = new SqlParameter("@id", id);
            db.Database.ExecuteSqlCommand(query, param);
            return RedirectToAction("List");
        }
    }
}
