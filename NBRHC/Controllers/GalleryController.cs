﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;
using System.Net;
using System.Web;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using NBRHC.Models;
using NBRHC.Data;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System.Diagnostics;

namespace NBRHC.Controllers
{
    //Note: This controller references Christine Bittle's http5204-blog-aspcore example.
    //Comments as well as code may look similar for learning purposes but is applied to this specific feature
    public class GalleryController : Controller
    {
        //Make a Db Context for CRUD
        private readonly NBRHCCMSContext db;
        //This is for file upload
        private readonly IHostingEnvironment _env;
        //We need the usermanager class to get things like the id or name
        private readonly UserManager<ApplicationUser> _userManager;
        //This function will return the current user at some point when called
        private async Task<ApplicationUser> GetCurrentUserAsync() => await _userManager.GetUserAsync(HttpContext.User);

        public GalleryController(NBRHCCMSContext context, UserManager<ApplicationUser> usermanager, IHostingEnvironment env)
        {
            //When gallery controller is created, it creates three things
            //Db Context for CRUD
            db = context;
            //User manager for getting the current logged in user
            _userManager = usermanager;
            //Hosting environment for getting filepaths
            _env = env;
        }

        public async Task<int> GetUserDetails(ApplicationUser user)
        {
            //UserState
            //0 => Public user 
            //1 => User but has no admin account 
            //2 => User has admin account 
            if (user == null) return 0;
            if (user.AdminID == null) return 1;  
            else
            {
                return 2; 
            }
        }

        // GET: Gallery
        public ActionResult Index()
        {
            //If the app calls for index of gallery redirect to Views/List.cshtml
            return RedirectToAction("List");
        }

        public async Task<ActionResult> List(int pagenum)
        {            
            //Get current user
            var user = await GetCurrentUserAsync();
            var userstate = await GetUserDetails(user);

            //Info on user
            ViewData["UserState"] = userstate;
            ViewData["UserAdminID"] = 0;

            //Pagination
            //Referenced from:
            //Christine Bittle's http5204-blog-aspcore example BlogController.cs line 135
            var _gallery = await db.Gallery.ToListAsync(); //Get a list of our rows in the gallery table
            int imagecount = _gallery.Count(); //Set total number of rows
            int perpage = 5; //Set number of rows we want max to show
            int maxpage = (int)Math.Ceiling((decimal)imagecount / perpage) - 1; //Max shown per page, Math.Ceiling rounds up
            if (maxpage < 0) maxpage = 0; //Bounds
            if (pagenum < 0) pagenum = 0; //Bounds
            if (pagenum > maxpage) pagenum = maxpage; //Bounds
            int start = perpage * pagenum;
            //Show page numbers and which page the user is on
            ViewData["pagenum"] = (int)pagenum; 
            ViewData["PaginationSummary"] = "";
            if (maxpage > 0)
            {
                ViewData["PaginationSummary"] =
                    (pagenum + 1).ToString() + " of " +
                    (maxpage + 1).ToString();
            }

            if(userstate == 2)
            {
                ViewData["UserAdminID"] = user.AdminID;
            }
            
            return View(await db.Gallery.Skip(start).Take(perpage).ToListAsync());
        }

        // GET: Gallery/Details
        public async Task<ActionResult> Details(int? id)
        {
            //Get our current user
            var user = await GetCurrentUserAsync();
            var userstate = await GetUserDetails(user);
            ViewData["UserState"] = userstate;

            //If user is not logged in 
            if (userstate == 0)
            {
                return new StatusCodeResult(400);
            }

            if(userstate == 2)
            {
                ViewData["UserAdminID"] = user.AdminID;
            }

            return View(await db.Gallery.SingleOrDefaultAsync(i => i.ImageID == id));
        }

        // GET: Gallery/Create
        public ActionResult New()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> New([Bind("ImageID, ImageAlt, ImageName")] Gallery gallery, IFormFile galleryimg)
        {
            //Referenced from Christine Bittle's AuthorController.cs http5204-blog-aspcore
            var user = await GetCurrentUserAsync(); //Get current user
            var userstate = await GetUserDetails(user);
            if (userstate == 0) return Forbid(); //Forbid because they don't have an account
            if (userstate == 1) return Forbid(); //Forbid because they are not an admin

            var webRoot = _env.WebRootPath;

            if(galleryimg != null)
            {
                if (galleryimg.Length > 0)
                {
                    //file extensioncheck taken from https://www.c-sharpcorner.com/article/file-upload-extension-validation-in-asp-net-mvc-and-javascript/
                    var valtypes = new[] { "jpeg", "jpg", "png", "gif" };
                    var extension = Path.GetExtension(galleryimg.FileName).Substring(1);

                    if (valtypes.Contains(extension))
                    {
                        //Get file name
                        string pic = galleryimg.FileName;
                        //Set ImageName to pictures file name
                        gallery.ImageName = pic;

                        //string fn is qual to filename.extension
                        string fn = pic + "." + extension;

                        //Get direct path to wwwroot/images/gallery
                        string path = Path.Combine(webRoot, "images/gallery");
                        path = Path.Combine(path, fn);

                        //Save the file
                        using (var stream = new FileStream(path, FileMode.Create))
                        {
                            galleryimg.CopyTo(stream);
                        }
                        //let the model know that there is a picture with an extension
                        gallery.ImageType = extension;

                    }
                    else
                    {
                        TempData["AddFail"] = "Failed to add image";
                        return RedirectToAction("Index");
                    }
                }
            }
            db.Gallery.Add(gallery);
            db.SaveChanges();
            TempData["AddSuccess"] = "Image successfully added";
            return RedirectToAction("Index");

        }

        // GET: Gallery/Edit
        public async Task<ActionResult> Edit(int? id)
        {
            //Get current user, if not logged in or not an admin then forbid
            var user = await GetCurrentUserAsync();
            var userstate = await GetUserDetails(user);

            //If user not logged in 
            if (userstate == 0)
            {
                return new StatusCodeResult(400);
            }
            
            //Find specific image in the gallery we are editing
            Gallery gallery = db.Gallery.Find(id);

            if (gallery == null)
            {
                return NotFound();
            }

            if (userstate == 0)
            {
                return Forbid();
            }
            if (userstate == 1)
            {
                return Forbid();
            }

            return View(gallery);

        }

        // POST: Gallery/Edit
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind("ImageID, ImageAlt, ImageName, ImageType")] Gallery gallery, IFormFile galleryimg)
        {
            var user = await GetCurrentUserAsync();
            var userstate = await GetUserDetails(user);

            //Must be logged in 
            if (userstate == 0)
            {
                return Forbid();
            }
            //If the user is not an admin they cannot edit the gallery
            if (userstate == 1)
            {
                return Forbid();
            }

            var webRoot = _env.WebRootPath;

            if (galleryimg != null)
            {
                if (galleryimg.Length > 0)
                {
                    //file extensioncheck taken from https://www.c-sharpcorner.com/article/file-upload-extension-validation-in-asp-net-mvc-and-javascript/
                    var valtypes = new[] { "jpeg", "jpg", "png", "gif" };
                    var extension = Path.GetExtension(galleryimg.FileName).Substring(1);

                    if (valtypes.Contains(extension))
                    {
                        //Get file name
                        string pic = galleryimg.FileName;
                        //Set ImageName to pictures file name
                        gallery.ImageName = pic;

                        //string fn is qual to filename.extension
                        string fn = pic + "." + extension;

                        //Get direct path to wwwroot/images/gallery
                        string path = Path.Combine(webRoot, "images/gallery");
                        path = Path.Combine(path, fn);

                        //Save the file
                        using (var stream = new FileStream(path, FileMode.Create))
                        {
                            galleryimg.CopyTo(stream);
                        }
                        //let the model know that there is a picture with an extension
                        gallery.ImageType = extension;

                    }
                    else
                    {
                        TempData["EditFail"] = "Failed to edit image";
                        return RedirectToAction("Details/" + gallery.ImageID);
                    }
                }
            }
                db.Entry(gallery).State = EntityState.Modified;
                db.SaveChanges();
                TempData["EditSuccess"] = "Image successfully edited";
                return RedirectToAction("Details/" + gallery.ImageID);

        }

        // Gallery/Delete
        public async Task<ActionResult> Delete(int? id)
        {
            //Find specific image id
            Gallery gallery = db.Gallery.Find(id);
            if (id == null || gallery == null)
            {
                return NotFound();
            }
            else
            {
                var user = await GetCurrentUserAsync();
                var userstate = await GetUserDetails(user);

                //If user is not logged in or is not an admin do not allow them to delete
                //Even if user is logged in but not admin they will not be able to delete
                if (userstate == 0 || userstate == 1)
                {
                    return Forbid();
                }
                else
                {
                    db.Gallery.Remove(gallery);
                    db.SaveChanges();
                    var webRoot = _env.WebRootPath;
                    string path = Path.Combine(webRoot, "images/gallery/");
                    path = Path.Combine(path, gallery.ImageName + "." + gallery.ImageType);
                    //Referenced https://stackoverflow.com/questions/37204268/how-to-delete-the-uploaded-files-in-the-app-data-uploads-folder-asp-net-mvc
                    if (System.IO.File.Exists(path))
                    {
                        System.IO.File.Delete(path);
                    }
                    TempData["DeleteSuccess"] = "Image successfully deleted";
                    return RedirectToAction("List");
                }
            }
        }



    }
}