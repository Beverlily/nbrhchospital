﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;
using System.Net;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using NBRHC.Models;
using NBRHC.Models.ViewModels;
using NBRHC.Data;
using System.Diagnostics;


namespace NBRHC.Controllers
{
    public class EmployeeController : Controller
    {
        private readonly NBRHCCMSContext db;
        private readonly UserManager<ApplicationUser> _userManager;
        private async Task<ApplicationUser> GetCurrentUserAsync() => await _userManager.GetUserAsync(HttpContext.User);


        public EmployeeController(NBRHCCMSContext context, UserManager<ApplicationUser> usermanager)
        {
            db = context;
            _userManager = usermanager;
        }
        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        public async Task<ActionResult> List()
        {
            var user = await GetCurrentUserAsync();
            if (user == null) return Forbid();

            List<Employee> employees = db.Employees.ToList();
            return View(employees);
        }

        public ActionResult Thanks()
        {

            return View();
        }

        public ActionResult New()
        {
            
            EmployeeEdit employeeeditview = new EmployeeEdit();

            
            employeeeditview.Jobs = db.Jobs.ToList();

            //GOTO Views/Employee/New.cshtml
            return View(employeeeditview);
        }

        [HttpPost]
        public ActionResult New(string EmployeeFName_New, string EmployeeLName_New, string EmailID_New, int PhoneNumber_New, string StreetName_New,
                                string City_New,string PostalCode_New,int EmployeeJob_New)
        {
              
            string query = "insert into employees (EmployeeFName,EmployeeLName,EmailID,PhoneNumber,StreetName,City,PostalCode,JobID) values (@firstname,@lastname,@email,@phonenumber,@street,@city,@postalcode,@job)";

            
            SqlParameter[] myparams = new SqlParameter[8];
            //@title paramter
            myparams[0] = new SqlParameter("@firstname", EmployeeFName_New);
            //@bio parameter
            myparams[1] = new SqlParameter("@lastname", EmployeeLName_New);

            myparams[2] = new SqlParameter("@email", EmailID_New);
            //@bio parameter
            myparams[3] = new SqlParameter("@phonenumber", PhoneNumber_New);

            myparams[4] = new SqlParameter("@street", StreetName_New);
            //@bio parameter
            myparams[5] = new SqlParameter("@city", City_New);
            myparams[6] = new SqlParameter("@postalcode", PostalCode_New);
            //@author (id) FOREIGN KEY paramter
            myparams[7] = new SqlParameter("@job", EmployeeJob_New);

            
            db.Database.ExecuteSqlCommand(query, myparams);

            //GOTO: 
            return RedirectToAction("Thanks");
        }
    }
}