﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;
using System.Net;
using System.Web;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using NBRHC.Models;
using NBRHC.Data;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System.Diagnostics;

namespace NBRHC.Controllers
{
    public class DonorController : Controller
    {
        private readonly NBRHCCMSContext db;

        //Dependencies to be able to get the current user
        //I used the code from Christine's example to be able to build the login functionality...
        private readonly UserManager<ApplicationUser> userManager;
        private async Task<ApplicationUser> GetCurrentUserAsync() => await userManager.GetUserAsync(HttpContext.User);


        public DonorController(NBRHCCMSContext context, UserManager<ApplicationUser> usermgr)
        {
            db = context;
            userManager = usermgr;
        }

        public async Task<IActionResult> Index()
        {
            return View(await db.Donations.ToListAsync());
        }

        public int GetUserState(ApplicationUser user)
        {
            if (user == null)
            {
                return 0;
            }
            else if (user.AdminID == null)
            {
                return 1;
            }
            else
            {
                var useradminid = user.AdminID;
                return 2;
            }
        }
        

        public async Task<ActionResult> Show(int id)
        {
            var user = await GetCurrentUserAsync();
            var userstate = GetUserState(user);

            if (userstate == 0)
            {
                ViewData["UserState"] = "guest";
            }
            if (userstate == 1)
            {
                ViewData["UserState"] = "user";
            }
            if (userstate == 2)
            {
                ViewData["UserState"] = "admin";
            }

            var donor = db.Donors.Where(d => d.DonorId == id).FirstOrDefault();
            return View(donor);
        }

        public async Task<ActionResult> New()
        {
            var user = await GetCurrentUserAsync();
            var userstate = GetUserState(user);

            Debug.WriteLine("User state: " + userstate);

            if (userstate == 0)
            {
                return RedirectToAction("List");
            }
            else
            {
                return View();
            }
            
        }
        
        public async Task<ActionResult> Create(string formfirstname, string formlastname, string formaddress,
            string formhomephone, string formcellphone)
        {
            var user = await GetCurrentUserAsync();
            var userstate = GetUserState(user);

            Debug.WriteLine("User state: " + userstate);
            Debug.WriteLine("Firstname: " + formfirstname);
            Debug.WriteLine("Lastname: " + formlastname);
            Debug.WriteLine("Address: " + formaddress);
            Debug.WriteLine("Home phone: " + formhomephone);
            Debug.WriteLine("Cell phone: " + formcellphone);

            if (userstate == 0)
            {
                return RedirectToAction("List");
            }
            else
            {
                var query = "INSERT INTO donors (firstName, lastName, address, homePhoneNo, cellPhoneNo)";
                query += "VALUES (@first, @last, @addr, @homephone, @cellphone)";

                SqlParameter[] sqlParams = new SqlParameter[5];
                sqlParams[0] = new SqlParameter("@first", formfirstname);
                sqlParams[1] = new SqlParameter("@last", formlastname);
                sqlParams[2] = new SqlParameter("@addr", formaddress);
                sqlParams[3] = new SqlParameter("@homephone", formhomephone);
                sqlParams[4] = new SqlParameter("@cellphone", formcellphone);

                db.Database.ExecuteSqlCommand(query, sqlParams);
                return RedirectToAction("List");
            }
        }

        public async Task<ActionResult> List(int pagenum)
        {
            var user = await GetCurrentUserAsync();
            var userstate = GetUserState(user);

            if (userstate == 0)
            {
                ViewData["UserState"] = "Guest";
            }
            if (userstate == 1)
            {
                ViewData["UserState"] = "User";
            }
            if (userstate == 2)
            {
                ViewData["UserState"] = "Admin";
            }

            //Code for the pagination is pretty much the same as the example
            //except for some of the names and I had to work with a few
            //different relationships between the data.

            //Get the list of donors, we only need this because we need
            //to get the count right after.
            var donors = await db.Donors.ToListAsync();
            //2 Get the count of donors.
            int donorcount = donors.Count();
            //3 add a perpage, sets the number of donations per page.
            int perpage = 5;
            //Dividing total number of donations with perpage, and using Math.ceiling
            //to determine the total number of pages.
            int maxpage = (int)Math.Ceiling((decimal)donorcount / perpage) - 1;
            //Determine the bounds of our code
            if (maxpage < 0)
            {
                maxpage = 0;
            }

            if (pagenum < 0)
            {
                pagenum = 0;
            }
            if (pagenum > maxpage)
            {
                pagenum = maxpage;
            }

            //Skip the first X pages where X is number of pages times the number of items per page.
            int start = perpage * pagenum;
            //Sending information to the view
            ViewData["pagenum"] = (int)pagenum;
            ViewData["PaginationSummary"] = "";
            //Sending information to the view if the view is not on the first page of results.
            if (maxpage > 0)
            {
                ViewData["PaginationSummary"] =
                    (pagenum + 1).ToString() + " of " + (maxpage + 1).ToString();
            }
            //Like an SQL query we list the donations that we need on each page
            //.skip: Offset query by this many records
            //.take: Take only this many records.
            donors = await db.Donors.Skip(start).Take(perpage).ToListAsync();

            return View(donors);
        }

        public async Task<ActionResult> Delete(int? id)
        {
            var user = await GetCurrentUserAsync();
            var userstate = GetUserState(user);

            Debug.WriteLine("User state: " + userstate);

            if (userstate == 0)
            {
                return RedirectToAction("List");
            }
            else
            {
                string query = "DELETE FROM donors WHERE DonorId = @id";
                SqlParameter param = new SqlParameter("@id", id);
                db.Database.ExecuteSqlCommand("List");
                return RedirectToAction("List");
            }
        }
        
        public async Task<ActionResult> Edit(int id)
        {
            var user = await GetCurrentUserAsync();
            var userstate = GetUserState(user);

            Debug.WriteLine("User state: " + userstate);

            if (userstate == 0)
            {
                return RedirectToAction("List");
            }
            else
            {
                var donor = db.Donors
                .Where(d => d.DonorId == id)
                .FirstOrDefault();

                return View(donor);
            }
        }

        [HttpPost]
        public async Task<ActionResult> Edit(int id, string firstname, string lastname, string homephone, string cellphone, string address)
        {
            if ((id == null) || (db.Donors.Find(id) == null))
            {
                //Show not found message
                return NotFound();
            }

            var user = await GetCurrentUserAsync();
            var userstate = GetUserState(user);

            Debug.WriteLine("User state: " + userstate);

            if (userstate == 0)
            {
                return RedirectToAction("List");
            }
            else
            {
                string query = "UPDATE donors";
                query += " SET address = @addr, cellPhoneNo = @cell, firstName = @first, homePhoneNo = @home, lastName = @last";
                query += " WHERE DonorId = @id";

                SqlParameter[] myparams = new SqlParameter[6];

                myparams[0] = new SqlParameter("@addr", address);
                myparams[1] = new SqlParameter("@cell", cellphone);
                myparams[2] = new SqlParameter("@first", firstname);
                myparams[3] = new SqlParameter("@home", homephone);
                myparams[4] = new SqlParameter("@last", lastname);
                myparams[5] = new SqlParameter("@id", id);

                db.Database.ExecuteSqlCommand(query, myparams);
                return RedirectToAction("Show/" + id);
            }
        }
    }
}
