﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;
using System.Net;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using NBRHC.Models;
using NBRHC.Data;
using System.Diagnostics;

//refered from christines code
namespace NBRHC.Controllers
{
  public class FoodServiceController : Controller
 {
 private readonly NBRHCCMSContext db;
 private readonly UserManager<ApplicationUser> _userManager;
 private async Task<ApplicationUser> GetCurrentUserAsync() => await _userManager.GetUserAsync(HttpContext.User);
 public FoodServiceController(NBRHCCMSContext context, UserManager<ApplicationUser> usermanager)
 {
  db = context;
  _userManager = usermanager;
  }
 public async Task<int> GetUserDetails(ApplicationUser user)
 {
   if (user == null) return 0;
   var userID = user.Id;
  if (user.AdminID == null) return 1;
 else{
   var fAdminId = user.AdminID;
   var userfood = await db.FoodServices.Where(f => f.AdminID == fAdminId).ToListAsync();
   var userfoodcount = userfood.Count();
   if (userfoodcount == 0) return 2;
   else if (userfoodcount > 0) return 3;
   }
    return -1;
   }
 public async Task<bool> IsUserFoodOwner(ApplicationUser user, int FoodID)
 {
   if (user == null) return false;
   if (user.AdminID == null) return false;
   var userfood = await
   db
   .FoodServices
   .Include(f=>f.Admin)
   .Where(f => f.AdminID == user.AdminID)
   .Where(f => f.FoodServiceID == FoodID)
   .ToListAsync();
   if (userfood.Count() > 0) return true;
   return false;
        }
 public ActionResult Index()
    {
       return RedirectToAction("List");
    }
 public async Task<ActionResult> List(int pagenum)
   {
     var food = await db.FoodServices.ToListAsync();
     int blogcount = food.Count();
     int perpage = 3;
     int maxpage = (int)Math.Ceiling((decimal)blogcount / perpage) - 1;
     if (maxpage < 0) maxpage = 0;
     if (pagenum < 0) pagenum = 0;
     if (pagenum > maxpage) pagenum = maxpage;
     int start = perpage * pagenum;
      ViewData["pagenum"] = (int)pagenum;
      ViewData["PaginationSummary"] = "";
      if (maxpage > 0)
       {
           ViewData["PaginationSummary"] =
           (pagenum + 1).ToString() + " of " +
           (maxpage + 1).ToString();
        }
    List<FoodService> foodServices = db.FoodServices.Include(f => f.Admin).ToList();
    var user = await GetCurrentUserAsync();
    if (user != null)
     {
      if (user.AdminID == null)
       {
          ViewData["UserHasAdmin"] = "False";
       }
     else
      {
        ViewData["UserHasAdmin"] = user.AdminID.ToString();
      }
   return View(foodServices);
        }
   else
     {
       ViewData["UserHasAdmin"] = "None";
       return View(foodServices);
   }
 }
   public async Task<ActionResult> Create()
    {
      var user = await GetCurrentUserAsync();
      var userstate = await GetUserDetails(user);
      switch (userstate)
        {
  case 0: return RedirectToAction("Login", "Account");
  case 1: return RedirectToAction("Create", "Admin");
         }
    ViewData["UserHasAdmin"] = user.AdminID;
    return View();
  }
 [HttpPost]
  public async Task<ActionResult> Create(string FoodName, string SpeciallyFor, string foodPrice, int FoodAdmin)
   {
     var user = await GetCurrentUserAsync();
     var userstate = await GetUserDetails(user);
     if (userstate < 2) return Forbid();
     if (user.AdminID != FoodAdmin)
  {
       return Forbid();
  }
  string query = "insert into FoodServices (FoodName,FoodPrice,PatientType,AdminID) " +
  "values (@name, @price,@type,@id)";

  SqlParameter[] myparams = new SqlParameter[4];
    myparams[0] = new SqlParameter("@name", FoodName);
    myparams[1] = new SqlParameter("@price", foodPrice);
    myparams[2] = new SqlParameter("@type", SpeciallyFor);
    myparams[3] = new SqlParameter("@id", FoodAdmin);
    db.Database.ExecuteSqlCommand(query, myparams);
    return RedirectToAction("Show");
  }
 public async Task<ActionResult> Show()
   {
     var user = await GetCurrentUserAsync();
     if (user != null)
       {
         if (user.AdminID == null){
         ViewData["UserHasAdmin"] = "False";
       }
     else{
          ViewData["UserHasAdmin"] = user.AdminID.ToString();
     }
   return View(await db.FoodServices.ToListAsync());
      }
 else {
      ViewData["UserHasAdmin"] = "1";
      return View(await db.FoodServices.ToListAsync());
      }
   }
 public async Task<ActionResult> Edit(int id)
     {
       var user = await GetCurrentUserAsync();
       var userstate = await GetUserDetails(user);
       bool isValid = await IsUserFoodOwner(user, id);
       if (!isValid) return Forbid();
       var myFood = db.FoodServices.Find(id); 
       if (myFood != null) return View(myFood);
       else return NotFound();
    }
[HttpPost]
   public async Task<ActionResult> Edit(int id, string FoodName, string SpeciallyFor, string foodPrice)
      {
        if (db.FoodServices.Find(id) == null)
      {
         return NotFound();
       }
 var user = await GetCurrentUserAsync();
 var userstate = await GetUserDetails(user);
 if (userstate == 3)
     {
       bool isValid = await IsUserFoodOwner(user, id);
        if (!isValid) return Forbid();
string query = "update FoodServices set FoodName=@name, FoodPrice=@price,  PatientType=@type " +
 "where FoodServiceID=@id";
 SqlParameter[] myparams = new SqlParameter[4];
 myparams[0] = new SqlParameter("@name", FoodName);
 myparams[1] = new SqlParameter("@type", SpeciallyFor);
 myparams[2] = new SqlParameter("@price", foodPrice);
 myparams[3] = new SqlParameter("@id", id);
db.Database.ExecuteSqlCommand(query, myparams);
return RedirectToAction("Show/" + id);
    }
 return Forbid();

 }
   public async Task<ActionResult> Delete(int id)
    {
     if (db.FoodServices.Find(id) == null)
         {
        return NotFound();

     }
   var user = await GetCurrentUserAsync();
  var userstate = await GetUserDetails(user);
   if (userstate == 3)
      {
    bool isValid = await IsUserFoodOwner(user, id);
    if (!isValid) return Forbid();
    string query = "delete from FoodServices where FoodServiceID = @id";
    SqlParameter param = new SqlParameter("@id", id);
    db.Database.ExecuteSqlCommand(query, param);
return RedirectToAction("Show");
  }
   return Forbid();
 }
 protected override void Dispose(bool disposing)
  {
   if (disposing)
       {
          db.Dispose();
            }
    base.Dispose(disposing);
  }
 }
}