﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;
using System.Net;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using NBRHC.Models;
using NBRHC.Models.ViewModels;
using NBRHC.Data;
using System.Diagnostics;

namespace NBRHC.Controllers
{
    public class JobController : Controller
    {
        private readonly NBRHCCMSContext db;
        private readonly UserManager<ApplicationUser> _userManager;
        private async Task<ApplicationUser> GetCurrentUserAsync() => await _userManager.GetUserAsync(HttpContext.User);

        public JobController(NBRHCCMSContext context, UserManager<ApplicationUser> usermanager)
        {
            db = context;
            _userManager = usermanager;
        }
        public async Task<int> GetUserDetails(ApplicationUser user)
        {
            //check if the user is an admin or not.
            //UserState
            //0 => Public user 
            //1 => User but has no admin 
            //2 => User has admin account 
            if (user == null) return 0;
            var userID = user.Id;
            if (user.AdminID == null) return 1; //User has no admin account 
            else
            {
                //User has admin acconut 
                return 2;

            }

        }

        public ActionResult Index()
        {
            return RedirectToAction("List");
        }
        //Reference from Christine code
        public async Task<ActionResult> List(int pagenum)
        {

            var jobslist = await db.Jobs.ToListAsync();
            int jobcount = jobslist.Count();
            int perpage = 2;
            int maxpage = (int)Math.Ceiling((decimal)jobcount / perpage) - 1;
            if (maxpage < 0) maxpage = 0;
            if (pagenum < 0) pagenum = 0;
            if (pagenum > maxpage) pagenum = maxpage;
            int start = perpage * pagenum;
            ViewData["pagenum"] = (int)pagenum;
            ViewData["PaginationSummary"] = "";
            if (maxpage > 0)
            {
                ViewData["PaginationSummary"] =
                    (pagenum + 1).ToString() + " of " +
                    (maxpage + 1).ToString();
            }

            var user = await GetCurrentUserAsync();
            var userstate = await GetUserDetails(user);
            ViewData["UserState"] = userstate;
            ViewData["UserAdminID"] = 0; //default to 0
            if (userstate == 2)
            {
                ViewData["UserAdminID"] = user.AdminID;

            }
            //if there is a user 
            if (userstate != 0)
            {
                 
                List<Job> joblist = await db.Jobs.Include(j => j.Admin).Skip(start).Take(perpage).ToListAsync();
                return View(joblist);
            }
            else
            {
                //Public website visitors can view all listed job 
                List<Job> joblist = await db.Jobs.Skip(start).Take(perpage).ToListAsync();
                return View(joblist);
            }


            //List<Job> jobs = await db.Jobs.Skip(start).Take(perpage).ToListAsync();
            //return View(jobs);
        }
        //public ActionResult New()
        //{

        //    JobEdit jobeditview = new JobEdit();

        //    //GOTO Views/Job/New.cshtml
        //    return View(jobeditview);
        //}
        public async Task<ActionResult> New()
        {
            //JobEdit jobeditview = new JobEdit();
            var user = await GetCurrentUserAsync();
            var userstate = await GetUserDetails(user);

            //Public website visitors and users who are not admins cannot create jobs 
            switch (userstate)
            {
                case 0: return RedirectToAction("Login", "Account");
                case 1: return RedirectToAction("Create", "Admin");
            }

            //The user is an admin 
            ViewData["UserAdminID"] = user.AdminID;
            ViewData["UserAdmin"] = user.Admin;

            //return View(jobeditview);
            return View();
        }

        [HttpPost]
        public ActionResult New(string JobTitle_New, string JobDescription_New, string Department_New, int AdminID, Admin Admin)
        {
             
            string query = "insert into jobs (JobTitle,JobDescription,Department,AdminID) values (@title,@description,@department,@adminid)";

            
            SqlParameter[] myparams = new SqlParameter[5];
            //@title paramter
            myparams[0] = new SqlParameter("@title", JobTitle_New);
            //@description parameter
            myparams[1] = new SqlParameter("@description", JobDescription_New);
            //@department parameter
            myparams[2] = new SqlParameter("@department", Department_New);

            myparams[3] = new SqlParameter("@adminid", AdminID);
            


            db.Database.ExecuteSqlCommand(query, myparams);
            //testing that the paramters do indeed pass to the method
            //Debug>Windows>Output
            Debug.WriteLine(query);

            return RedirectToAction("List");
         }

        //public ActionResult Edit(int id)
        //{

        //    JobEdit jobeditview = new JobEdit();
        //    jobeditview.Job = db.Jobs.SingleOrDefault(j => j.JobID == id); 
        //    return View(jobeditview);
        //}

        // Job/Edit
        public async Task<ActionResult> Edit(int id)
        {
            //job with that id doesn't exist 
            if (db.Jobs.Find(id) == null)
            {
                return NotFound();
            }
            else
            {
                Job job = db.Jobs.Find(id);
                var user = await GetCurrentUserAsync();
                var userstate = await GetUserDetails(user);

                //if user is admin 
                if (userstate == 2)
                {
                   
                   return View(job);

                }
                //Not admin so can't edit it 
                return Forbid();
            }
        }
        [HttpPost]
        public ActionResult Edit(int id, string JobTitle, string JobDescription, string Department,int AdminID,Admin Admin)
        {

            
            string query = "update jobs set JobTitle=@title, JobDescription=@description, Department=@department,AdminID=@adminid where jobid=@id";

            
            SqlParameter[] myparams = new SqlParameter[6];
            
            myparams[0] = new SqlParameter("@title", JobTitle);
            
            myparams[1] = new SqlParameter("@description", JobDescription);
            
            myparams[2] = new SqlParameter("@department", Department);

            myparams[3] = new SqlParameter("@id", id);
            myparams[4] = new SqlParameter("@adminid", AdminID);


            db.Database.ExecuteSqlCommand(query, myparams);
            Debug.WriteLine(query);

            return RedirectToAction("Show/" + id);
            //return RedirectToAction("List");
        }
        public ActionResult Show(int? id)
        {
            if ((id == null) || (db.Jobs.Find(id) == null))
            {
                return NotFound();

            }

            string query = "select * from jobs where jobid=@id";

            SqlParameter myparam = new SqlParameter("@id", id);
            
            Job job = db.Jobs.SingleOrDefault(j => j.JobID == id);

            return View(job);

        }

        public async Task<ActionResult>  Delete (int?id)
        {
            if ((id == null) || (db.Jobs.Find(id) == null))
            {
                return NotFound();

            }
            else
            {
                Job job = db.Jobs.Find(id);
                var user = await GetCurrentUserAsync();
                var userstate = await GetUserDetails(user);

                if (userstate == 2)
                {

                    string query = "delete from employees where JobID=@id";
                    SqlParameter param = new SqlParameter("@id", id);
                    db.Database.ExecuteSqlCommand(query, param);


                    query = "delete from Jobs where jobid=@id";
                    param = new SqlParameter("@id", id);
                    db.Database.ExecuteSqlCommand(query, param);
                    return RedirectToAction("List");
                }
                return Forbid();
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


    }
}