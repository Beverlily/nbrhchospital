﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace NBRHC.Controllers
{
    public class FoodClientController : Controller
    {
        string connString = ConnString.connString;
        // GET: FoodClient
        public ActionResult Index()
        {
            DataTable dt = new DataTable();
            using (SqlConnection sqlcon = new SqlConnection(connString))
            {
                sqlcon.Open();
                SqlDataAdapter sqlda = new SqlDataAdapter("SELECT * FROM food_services", sqlcon);
                sqlda.Fill(dt);
            }
            return View(dt);
        }

        // GET: FoodClient/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: FoodClient/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: FoodClient/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: FoodClient/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: FoodClient/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: FoodClient/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: FoodClient/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}