﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;
using System.Net;
using System.Web;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using NBRHC.Models;
using NBRHC.Models.ViewModels;
using NBRHC.Data;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System.Diagnostics;

namespace NBRHC.Controllers.Donations
{
    public class DonationController : Controller
    {
        //Database context
        private readonly NBRHCCMSContext db;

        //Dependencies to be able to get the current user
        //I used the code from Christine's example to be able to build the login functionality...
        private readonly UserManager<ApplicationUser> userManager;
        private async Task<ApplicationUser> GetCurrentUserAsync() => await userManager.GetUserAsync(HttpContext.User);


        public DonationController(NBRHCCMSContext context, UserManager<ApplicationUser> usermgr)
        {
            db = context;
            userManager = usermgr;
        }

        public async Task<IActionResult> Index()
        {
            return View(await db.Donations.ToListAsync());
        }

        public int GetUserState(ApplicationUser user)
        {
            //0: No user
            //1: User logged in
            //2: User logged in and is an admin

            if (user == null)
            {
                return 0;
            }
            else if(user.AdminID == null)
            {
                return 1;
            } else
            {
                var useradminid = user.AdminID;
                return 2;
            }
        }

        public async Task<ActionResult> Show(int id)
        {
            var user = await GetCurrentUserAsync();
            var userstate = GetUserState(user);

            Debug.WriteLine("User state: " + userstate);
            Debug.WriteLine("donation id: " + id);

            if (userstate == 0)
            {
                ViewData["UserState"] = "Guest";
            }
            if (userstate == 1)
            {
                ViewData["UserState"] = "User";
            }
            if (userstate == 2)
            {
                ViewData["UserState"] = "Admin";
            }

            var donation =
               db.Donations
               .Include(d => d.Donor)
               .Include(d => d.DonationGoal)
               .Where(d => d.DonationId == id)
               .FirstOrDefault();
            
            return View(donation);
        }

        public async Task<ActionResult> New()
        {
            var user = await GetCurrentUserAsync();
            var userstate = GetUserState(user);

            Debug.WriteLine("User state: " + userstate);

            if (userstate == 0)
            {
                return RedirectToAction("List");
            }
            else
            {
                //Ideally only an admin can add a new  user but to make it easier to add
                //stuff for testing purposes, I'm making it so a user can add a donation.
                DonationAdd donationaddview = new DonationAdd();
                donationaddview.donors = db.Donors.ToList();
                donationaddview.donationgoals = db.DonationGoals.ToList();
                return View(donationaddview);
            }
        }
        
        [HttpPost]
        //This actually does the New action.
        //We use HttpPost to specify the that the form is using post.
        //MAKE SURE the parameters match the name of the html name attributes.
        public async Task<ActionResult> Create(int formamt, int donorid, int donationgoalid)
        {
            var user = await GetCurrentUserAsync();
            var userstate = GetUserState(user);

            Debug.WriteLine("User state: " + userstate);
            Debug.WriteLine("Form amt: " + formamt);
            Debug.WriteLine("Donor id: " + donorid);
            Debug.WriteLine(DateTime.Today.ToString("yyyy-MM-dd"));

            if (userstate == 0)
            {
                return RedirectToAction("List");
            }
            else
            {
                //QUERY
                //second thing, build a query that inserts a new donation into the donations table
                var query = "INSERT INTO donations (Amount, Date, DonationGoalId, DonorId)";
                query += "VALUES (@amt, @dat, @dgid, @did)";
                SqlParameter[] sqlparams = new SqlParameter[4];
                sqlparams[0] = new SqlParameter("@amt", formamt);
                sqlparams[1] = new SqlParameter("@dat", DateTime.Today.ToString("yyyy-MM-dd"));
                sqlparams[2] = new SqlParameter("@dgid", donationgoalid);
                sqlparams[3] = new SqlParameter("@did", donorid);

                //ALGORITHM
                //third thing, run the query into the database
                db.Database.ExecuteSqlCommand(query, sqlparams);
                return RedirectToAction("List");
            }
        }

        public async Task<ActionResult> List(int pagenum)
        {
            var user = await GetCurrentUserAsync();
            var userstate = GetUserState(user);

            Debug.WriteLine("User state: " + userstate);

            if (userstate == 0)
            {
                ViewData["UserState"] = "Guest";
            }
            if (userstate == 1)
            {
                ViewData["UserState"] = "User";
            }
            if (userstate == 2)
            {
                ViewData["UserState"] = "Admin";
            }

            /**********************PAGINATION****************************/

            var donations = await db.Donations.Include(d => d.DonationGoal).ToListAsync();
            //var _donors = await _context.Donations.Include(d => d.Donor).ToListAsync();
            //2 Get the size of the query of all records.
            int donationcount = donations.Count();
            //add a perpage, sets the number of donations per page.
            int perpage = 5;
            //Dividing total number of donations with perpage, and using Math.ceiling
            //to determine the total number of pages.
            int maxpage = (int)Math.Ceiling((decimal)donationcount / perpage) - 1;
            //Making sure we don't go to a page that's out of bounds
            if (maxpage < 0)
                { maxpage = 0; }
            if (pagenum < 0)
                { pagenum = 0; }
            if (pagenum > maxpage)
                { pagenum = maxpage; }
            //Skip the first X pages where X is number of pages timesnumber of items per page.
            int start = perpage * pagenum;
            //Sending information to the view
            ViewData["pagenum"] = (int)pagenum;
            ViewData["PaginationSummary"] = "";
            //Sending information to the view if the view is not on the first page of results.
            if (maxpage > 0)
            {
                ViewData["PaginationSummary"] =
                    (pagenum + 1).ToString() + " of " + (maxpage + 1).ToString();
            }
            //Like an SQL query we list the donations that we need on each page
            //.skip: Offset query by this many records
            //.take: Take only this many records.
            donations = await db.Donations.Include(d => d.Donor).Skip(start).Take(perpage).ToListAsync();
            return View(donations);
        }

        public async Task<ActionResult> Delete(int? id)
        {
            //Ideally only an admin can delete donations but I'm making it
            //so users can too because it's convenient for testing...
            var user = await GetCurrentUserAsync();
            var userstate = GetUserState(user);

            Debug.WriteLine("User state: " + userstate);

            if (userstate == 0)
            {
                return RedirectToAction("List");
            } else if (userstate == 1)
            {
                string query = "DELETE FROM donations WHERE DonationID = @id";
                SqlParameter param = new SqlParameter("@id", id);
                db.Database.ExecuteSqlCommand(query, param);
                return RedirectToAction("List");
            } else
            {
                string query = "DELETE FROM donations WHERE DonationID = @id";
                SqlParameter param = new SqlParameter("@id", id);
                db.Database.ExecuteSqlCommand(query, param);
                return RedirectToAction("List");
            }

            

        }

        /*The method that shows the edit page*/
        
        public async Task<ActionResult> Edit(int id)
        {
            var user = await GetCurrentUserAsync();
            var userstate = GetUserState(user);

            Debug.WriteLine("User state: " + userstate);

            if (userstate == 0)
            {
                return RedirectToAction("List");
            }
            else
            {
                DonationEdit donationedit = new DonationEdit();
                donationedit.DonationGoals = db.DonationGoals.ToList();
                donationedit.Donors = db.Donors.ToList();

                donationedit.Donation = db.Donations
                    .Where(d => d.DonationId == id)
                    .FirstOrDefault();

                return View(donationedit);
            }
        }

        /*This does the edit operation*/
        [HttpPost]
        public async Task <ActionResult> Edit(int id, int amount, string date, int donationgoalid, int donorid)
        {
            var user = await GetCurrentUserAsync();
            var userstate = GetUserState(user);

            Debug.WriteLine("User state: " + userstate);

            //Validation
            if ((id == null) || (db.Donations.Find(id) == null))
            {
                return NotFound();
            }

            if (userstate == 0)
            {
                return RedirectToAction("List");
            }
            else
            {
                if (userstate == 1)
                {
                    ViewData["UserState"] = "User";
                }
                if (userstate == 2)
                {
                    ViewData["UserState"] = "Admin";
                }

                string query = "UPDATE donations" +
                " SET Amount = @amt, date = @date, " +
                " DonationGoalId = @dgid, DonorId = @dnrid" +
                " WHERE DonationId = @dntnid";

                Debug.WriteLine("Donation id is " + id);
                Debug.WriteLine("Amount is " + amount);
                Debug.WriteLine("Date is " + date);
                Debug.WriteLine("Donation goal id is " + donationgoalid);
                Debug.WriteLine("Donor id is " + donorid);
                Debug.WriteLine("The query is: " + query);

                SqlParameter[] myparams = new SqlParameter[5];
                myparams[0] = new SqlParameter("@amt", amount);
                myparams[1] = new SqlParameter("@date", date);
                myparams[2] = new SqlParameter("@dgid", donationgoalid);
                myparams[3] = new SqlParameter("@dnrid", donorid);
                myparams[4] = new SqlParameter("@dntnid", id);

                db.Database.ExecuteSqlCommand(query, myparams);
                return RedirectToAction("Show/" + id);
            }

            
        }
    }
}
