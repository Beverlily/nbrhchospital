﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NBRHC.Models;

namespace NBRHC.Controllers
{
    public class FoodController : Controller
    {
        string connString = ConnString.connString;
        // GET: Food
        public ActionResult Index()
        {
            DataTable dt = new DataTable();
            using (SqlConnection sqlcon = new SqlConnection(connString))
            {
                sqlcon.Open();
                SqlDataAdapter sqlda = new SqlDataAdapter("SELECT * FROM food_services", sqlcon);
                sqlda.Fill(dt);
            }
            return View(dt);
        }

        // GET: Food/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Food/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Food/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(FoodModel food)
        {
            using (SqlConnection sqlcon = new SqlConnection(connString))
            {
                sqlcon.Open();
                string quary = "INSERT INTO food_services VALUES(@title,@des)";
                SqlCommand sqlcmd = new SqlCommand(quary, sqlcon);
                sqlcmd.Parameters.AddWithValue("@title", food.title );
                sqlcmd.Parameters.AddWithValue("@des", food.des );
                sqlcmd.ExecuteNonQuery();
            }
            return RedirectToAction("Create");
        }

        // GET: Food/Edit/5
        public ActionResult Edit(int id)
        {
            FoodModel fd = new FoodModel();
            DataTable datatable = new DataTable();
            using (SqlConnection sqlcon = new SqlConnection(connString))
            {
                sqlcon.Open();
                SqlDataAdapter sqlda = new SqlDataAdapter("SELECT * FROM food_services where service_id = @Id", sqlcon);
                sqlda.SelectCommand.Parameters.AddWithValue("@Id", id);
                sqlda.Fill(datatable);
            }
            if (datatable.Rows.Count == 1)
            {
                fd.Id = Convert.ToInt32(datatable.Rows[0][0].ToString());
                fd.title = datatable.Rows[0][1].ToString();
                fd.des = datatable.Rows[0][2].ToString();
                return View(fd);
            }
            else
                return RedirectToAction("Index");
        }

        // POST: Food/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(FoodModel fd)
        {
            using (SqlConnection sqlcon = new SqlConnection(connString))
            {
                sqlcon.Open();
                string quary = "Update food_services SET title=@Title , description=@Des WHERE service_id=@Id ";
                SqlCommand sqlcmd = new SqlCommand(quary, sqlcon);
                sqlcmd.Parameters.AddWithValue("@Id", fd.Id);
                sqlcmd.Parameters.AddWithValue("@Title", fd.title);
                sqlcmd.Parameters.AddWithValue("@Des", fd.des);
                sqlcmd.ExecuteNonQuery();
            }
            return RedirectToAction("Index");
        }

        // GET: Food/Delete/5
        public ActionResult Delete(int id)
        {
            using (SqlConnection sqlcon = new SqlConnection(connString))
            {
                sqlcon.Open();
                string quary = "DELETE FROM food_services WHERE service_id=@Id ";
                SqlCommand sqlcmd = new SqlCommand(quary, sqlcon);
                sqlcmd.Parameters.AddWithValue("@Id", id);
                sqlcmd.ExecuteNonQuery();
            }
            return RedirectToAction("Index");
        }

        // POST: Food/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}