﻿//Referenced Christine's code
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;
using System.Net;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using NBRHC.Models;
using NBRHC.Data;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System.Diagnostics;

namespace NBRHC.Controllers
{
    public class PageController : Controller
    {
        private readonly NBRHCCMSContext db;
        private readonly UserManager<ApplicationUser> _userManager;
        private async Task<ApplicationUser> GetCurrentUserAsync() => await _userManager.GetUserAsync(HttpContext.User);

        public PageController(NBRHCCMSContext context, UserManager<ApplicationUser> usermanager)
        {
            //Page controller needs the database context (for interacting with the database)
            //The usermanager (for getting the current logged in user)
            //User Manager allows you to manage application users 
            //lets you get things like id... i.e. var userId = user?.Id;

            db = context;
            _userManager = usermanager;
        }

        public async Task<int> GetUserDetails(ApplicationUser user)
        {
            //UserState
            //0 => Public user 
            //1 => User but has no admin account 
            //2 => User has admin account but hasn't created any pages 
            //3 => User has admin account and has created one or more pages 
            if (user == null) return 0;
            if (user.AdminID == null) return 1; //User has no admin account 
            else
            {
                //User has admin account
                var userPages = await db.Pages.Where(p => p.AdminID == user.AdminID).ToListAsync();
                var userPageCount = userPages.Count();
                if (userPageCount == 0) return 2; //User has no pages 
                else if (userPageCount > 0) return 3; //User has one or more pages 
            }
            return -1; //something went wrong
        }

        public async Task<bool> IsUserPageOwner(ApplicationUser user, int PageID)
        {
            //public user 
            if (user == null) return false;
            //User but no admin account 
            if (user.AdminID == null) return false;
        
            var userPages = await
                db
                .Pages
                .Include(p => p.Admin)
                .Where(p => p.AdminID == user.AdminID)
                .Where(p => p.PageID == PageID)
                .ToListAsync();
            if (userPages.Count() > 0) return true; //user is the owner of the page 
            return false;
        }

        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        public async Task<ActionResult> List(int pageNum)
        {
            var user = await GetCurrentUserAsync();
            var userstate = await GetUserDetails(user);

            //information about the type of user 
            ViewData["UserState"] = userstate;
            ViewData["UserAdminID"] = 0; //default 0
            List<Page> pages;

            if (userstate > 0)
            {
                //if there is a user 
                //Need to include information about the admin associated to the page 
                pages = await db.Pages.Include(p => p.Admin).ToListAsync();
            }
            else
            {
                //Public website visitors can only view published pages 
                pages = await db.Pages.Where(p => p.PagePublish.Equals(true)).ToListAsync();
            }

            /*Pagination - Referenced Christines code */
            int pageCount = pages.Count();
            int perpage = 10;
            int maxpage = (int)Math.Ceiling((decimal)pageCount / perpage) - 1;
            if (maxpage < 0) maxpage = 0;
            if (pageNum < 0) pageNum = 0;
            if (pageNum > maxpage) pageNum = maxpage;
            int start = perpage * pageNum;
            ViewData["pagenum"] = (int)pageNum;
            ViewData["PaginationSummary"] = "";
            if (maxpage > 0)
            {
                ViewData["PaginationSummary"] =
                    (pageNum + 1).ToString() + " of " +
                    (maxpage + 1).ToString();
            }

            if (userstate == 3)
            {
                //if user has an admin account
                ViewData["UserAdminID"] = user.AdminID;
            }

            return View(pages.Skip(start).Take(perpage).ToList());
        }


        public async Task<ActionResult> New()
        {
            var user = await GetCurrentUserAsync();
            var userstate = await GetUserDetails(user);

            //Public website visitors and users who are not admins cannot create pages 
            switch (userstate)
            {
                case 0: return RedirectToAction("Login", "Account"); 
                case 1: return RedirectToAction("Create", "Admin"); 
            }

            //The user is an admin 
            ViewData["UserAdminID"] = user.AdminID;
           
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> New(string PageTitle_New, string PageContent_New, bool PagePublish_New, int PageAdminID_New)
        {
            //Check user
            var user = await GetCurrentUserAsync();
            var userstate = await GetUserDetails(user);

            //Can't make a page unless you're an admin
            if (userstate < 2 ||user.AdminID != PageAdminID_New)
            {
                return Forbid();
            }

            //Query
            string query = "INSERT into Pages (PageTitle, PageContent, PagePublish, AdminID)" +
                        "VALUES (@title, @content, @publish, @admin)";

            SqlParameter[] parameters = {
                new SqlParameter("@title", PageTitle_New),
                new SqlParameter("@content", PageContent_New),
                new SqlParameter("@publish", PagePublish_New),
                new SqlParameter("@admin", PageAdminID_New),
            };

            db.Database.ExecuteSqlCommand(query, parameters);
            return RedirectToAction("List");
        }

        //Page/Details/
        public async Task<ActionResult> Details(int id)
        {
            //page with that id doesn't exist 
            if (db.Pages.Find(id) == null)
            {
                return NotFound();
            }
            else
            {
                Page page = db.Pages.Find(id);
                page.Admin = db.Admins.Find(page.AdminID);
                var user = await GetCurrentUserAsync();
                var userstate = await GetUserDetails(user);
                ViewData["UserState"] = userstate;
                ViewData["IsUserPageOwner"] = false;
                
                //Admin has pages so check if they're the admin of this page 
                if (userstate == 3)
                {
                    ViewData["IsUserPageOwner"] = await IsUserPageOwner(user, id);
                }
        
                //If user is not a public user 
                if (userstate > 0)
                {
                    return View(page);
                }
                else
                {
                    //User is a public user 
                    //Public website visitors cannot access pages that aren't published 
                    if (page.PagePublish == true)
                    {
                        return View(page);
                    }
                    else
                    {
                        //Page is not published, public user cannot view it
                        return Forbid();
                        //return BadRequest("Page was not found or you do not have perimission to access this page.");
                    }
                }
            }
        }

        // Page/Edit
        public async Task<ActionResult> Edit(int id)
        {
            //check if page with this id exists
            if (db.Pages.Find(id) == null)
            {
                return NotFound();
            }
            else
            {
                Page page = db.Pages.Find(id);
                var user = await GetCurrentUserAsync();
                var userstate = await GetUserDetails(user);

                //Admin has created pages, so check if they're the admin of this page 
                if (userstate == 3)
                {
                    bool pageOwner = await IsUserPageOwner(user, id);
                    if (pageOwner)
                    {
                        return View(page);
                    }
                    
                }
                //Not admin of this page so can't edit it 
                return Forbid();
            }
        }

        //POST: Page/Edit 
        [HttpPost]
        public async Task<ActionResult> Edit(int id, string PageTitle_Edit, string PageContent_Edit, bool PagePublish_Edit, int PageAdminID_Edit)
        {
            //check if page with this id exists
            if (db.Pages.Find(id) == null)
            {
                return NotFound();
            }
            var user = await GetCurrentUserAsync();
            var userstate = await GetUserDetails(user);
            if (userstate == 3)
            {
                //if user is an admin, checks if they are the page owner 
                bool pageOwner = await IsUserPageOwner(user, id);
                if (pageOwner)
                {
                    //User is page owner 
                    //Query
                    string query = "UPDATE Pages " +
                    "SET PageTitle=@title, PageContent=@content, PagePublish=@publish, AdminID=@admin " +
                    "WHERE PageID=@id";

                    SqlParameter[] parameters = {
                        new SqlParameter("@title", PageTitle_Edit),
                        new SqlParameter("@content", PageContent_Edit),
                        new SqlParameter("@publish", PagePublish_Edit),
                        new SqlParameter("@admin", PageAdminID_Edit),
                        new SqlParameter("@id", id),
                    };

                    db.Database.ExecuteSqlCommand(query, parameters);
                    return RedirectToAction("Details/" + id);
                }
            }
            //User does not have permission to edit page 
            return Forbid();
        }

        public async Task<ActionResult> Delete(int id)
        {
            //page with that id does not exist 
            if (db.Pages.Find(id) == null)
            {
                return NotFound();
            }
            else
            {
                Page page = db.Pages.Find(id);
                var user = await GetCurrentUserAsync();
                var userstate = await GetUserDetails(user);
               
                //User is an admin that has created pages 
                if (userstate == 3)
                {
                    //Check if user is admin of the page 
                    bool pageOwner = await IsUserPageOwner(user, id);
                    if (pageOwner)
                    {
                        //removes page if the user is the admin of the page 
                        db.Pages.Remove(page);
                        db.SaveChanges();
                        return RedirectToAction("List");
                    }
                }
                //can't delete because user is not admin of the page
                return Forbid();
            }
        }
    }
}
