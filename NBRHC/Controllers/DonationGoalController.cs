﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;
using System.Net;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using NBRHC.Models;
using NBRHC.Models.ViewModels;
using NBRHC.Data;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System.Diagnostics;

namespace NBRHC.Controllers
{
    public class DonationGoalController : Controller
    {
        private readonly NBRHCCMSContext db;

        //Dependencies to be able to get the current user
        //I used the code from Christine's example to be able to build the login functionality...
        private readonly UserManager<ApplicationUser> userManager;
        private async Task<ApplicationUser> GetCurrentUserAsync() => await userManager.GetUserAsync(HttpContext.User);


        public DonationGoalController(NBRHCCMSContext context, UserManager<ApplicationUser> usermgr)
        {
            db = context;
            userManager = usermgr;
        }

        public async Task<IActionResult> Index()
        {
            return View(await db.Donations.ToListAsync());
        }

        public int GetUserState(ApplicationUser user)
        {
            if (user == null)
            {
                return 0;
            }
            else if (user.AdminID == null)
            {
                return 1;
            }
            else
            {
                var useradminid = user.AdminID;
                return 2;
            }
        }

        

        public async Task<ActionResult> Show(int id)
        {
            var user = await GetCurrentUserAsync();
            var userstate = GetUserState(user);

            Debug.WriteLine("User state: " + userstate);
            Debug.WriteLine("donation id: " + id);

            if (userstate == 0)
            {
                ViewData["UserState"] = "Guest";
            }
            if (userstate == 1)
            {
                ViewData["UserState"] = "User";
            }
            if (userstate == 2)
            {
                ViewData["UserState"] = "Admin";
            }

            var donationgoal = db.DonationGoals.Where(d => d.DonationGoalId == id).FirstOrDefault();
            return View(donationgoal);
        }

        public async Task<ActionResult> New()
        {
            var user = await GetCurrentUserAsync();
            var userstate = GetUserState(user);

            Debug.WriteLine("User state: " + userstate);

            if (userstate == 0)
            {
                return RedirectToAction("List");
            }
            else
            {
                return View();
            }
        }

        [HttpPost]
        public async Task<ActionResult> Create(int formgoal, string formstartdate, string formenddate)
        {
            var user = await GetCurrentUserAsync();
            var userstate = GetUserState(user);

            Debug.WriteLine("User state: " + userstate);
            Debug.WriteLine("Goal: " + formgoal);
            Debug.WriteLine("Start date: " + formstartdate);
            Debug.WriteLine("End date:" + formenddate);

            if (userstate == 0)
            {
                return RedirectToAction("List");
            }
            else
            {
                var progress = 0;
                var WasMet = false;

                var query = "INSERT INTO DonationGoals (EndDate, Goal, Progress, StartDate, WasMet)";
                query += " VALUES (@enddt, @gol, @prog, @stdt, @met)";

                SqlParameter[] sqlParams = new SqlParameter[5];
                sqlParams[0] = new SqlParameter("@enddt", formenddate);
                sqlParams[1] = new SqlParameter("@gol", formgoal);
                sqlParams[2] = new SqlParameter("@prog", progress);
                sqlParams[3] = new SqlParameter("@stdt", formstartdate);
                sqlParams[4] = new SqlParameter("@met", WasMet);
                //The query expects the parameter @stdt which was not supplied
                db.Database.ExecuteSqlCommand(query, sqlParams);

                return RedirectToAction("List");
            }
        }

        public async Task<ActionResult> List(int pagenum)
        {
            var user = await GetCurrentUserAsync();
            var userstate = GetUserState(user);

            Debug.WriteLine("User state: " + userstate);

            if (userstate == 0)
            {
                ViewData["UserState"] = "Guest";
            }
            if (userstate == 1)
            {
                ViewData["UserState"] = "User";
            }
            if (userstate == 2)
            {
                ViewData["UserState"] = "Admin";
            }

            //Get the list of donation goals, we only need this right now because
            //we need the count right afterward.
            var donationgoals = await db.DonationGoals.ToListAsync();
            //2 Get the size of the query of all records.
            int donationgoalscount = donationgoals.Count();
            //add a perpage, sets the number of donations per page.
            int perpage = 5;
            //Dividing total number of donations with perpage, and using Math.ceiling
            //to determine the total number of pages.
            int maxpage = (int)Math.Ceiling((decimal)donationgoalscount / perpage) - 1;
            //Making sure we don't land on a page that doesn't exist (out of range)
            if (maxpage < 0)
            {
                maxpage = 0;
            }

            if (pagenum < 0)
            {
                pagenum = 0;
            }
            if (pagenum > maxpage)
            {
                pagenum = maxpage;
            }

            //Skip the first X pages where X is number of pages times the number of items per page.
            int start = perpage * pagenum;
            //Sending information to the view
            ViewData["pagenum"] = (int)pagenum;
            ViewData["PaginationSummary"] = "";
            //Sending information to the view if the view is not on the first page of results.
            if (maxpage > 0)
            {
                ViewData["PaginationSummary"] =
                    (pagenum + 1).ToString() + " of " + (maxpage + 1).ToString();
            }
            //Like an SQL query we list the donations that we need on each page
            //.skip: Offset query by this many records
            //.take: Take only this many records.
            donationgoals = await db.DonationGoals.Skip(start).Take(perpage).ToListAsync();

            return View(donationgoals);
        }

        public async Task<ActionResult> Delete(int? id)
        {
            var user = await GetCurrentUserAsync();
            var userstate = GetUserState(user);

            Debug.WriteLine("User state: " + userstate);

            if (userstate == 0)
            {
                return RedirectToAction("List");
            }
            else
            {
                string query = "DELETE FROM DonationGoals WHERE DonationGoalId = @id";
                SqlParameter param = new SqlParameter("@id", id);
                db.Database.ExecuteSqlCommand(query, param);
                return RedirectToAction("List");
            }
        }

        public async Task<ActionResult> Edit(int id)
        {
            var user = await GetCurrentUserAsync();
            var userstate = GetUserState(user);

            Debug.WriteLine("User state: " + userstate);

            if (userstate == 0)
            {
                return RedirectToAction("List");
            }
            else
            {
                var donationgoal =db.DonationGoals
                    .Where(d => d.DonationGoalId == id)
                    .FirstOrDefault();

                return View(donationgoal);
            }
        }

        [HttpPost]
        public async Task<ActionResult> Edit(int id, int goal, string startdate, string enddate)
        {
            var user = await GetCurrentUserAsync();
            var userstate = GetUserState(user);

            Debug.WriteLine("User state: " + userstate);

            /*Validation from the example...
             Is the id parameter null or is there no donation id in the db?*/
            if ((id == null) || (db.DonationGoals.Find(id) == null))
            {
                //Show error message
                return NotFound();

            }
            if (userstate == 0)
            {
                return RedirectToAction("List");
            }
            else
            {
                string query = "UPDATE DonationGoals" +
                " SET Goal = @goal," +
                " StartDate = @start, EndDate = @end" +
                " WHERE DonationGoalId = @id";

                Debug.WriteLine("ID is " + id);
                Debug.WriteLine("Goal is " + goal);
                Debug.WriteLine("Start date is " + startdate);
                Debug.WriteLine("End date is " + enddate);

                SqlParameter[] myparams = new SqlParameter[4];

                myparams[0] = new SqlParameter("@goal", goal);
                myparams[1] = new SqlParameter("@start", startdate);
                myparams[2] = new SqlParameter("@end", enddate);
                myparams[3] = new SqlParameter("@id", id);

                db.Database.ExecuteSqlCommand(query, myparams);
                return RedirectToAction("Show/" + id);
            }
        }
    }
}
