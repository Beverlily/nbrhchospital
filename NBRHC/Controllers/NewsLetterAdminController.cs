﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NBRHC.Models;

namespace NBRHC.Controllers
{
    public class NewsLetterAdminController : Controller
    {
        string connString = ConnString.connString;
        // GET: NewsLetterAdmin
        public ActionResult NewsLetterIndex()
        {
            DataTable dt = new DataTable();
            using (SqlConnection sqlcon = new SqlConnection(connString))
            {
                sqlcon.Open();
                SqlDataAdapter sqlda = new SqlDataAdapter("SELECT * FROM News_update", sqlcon);
                sqlda.Fill(dt);
            }
            return View(dt);
        }

        // GET: NewsLetterAdmin/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: NewsLetterAdmin/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: NewsLetterAdmin/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(NewsLetterAdminModel newsLetter)
        {
            using (SqlConnection sqlcon = new SqlConnection(connString))
            {
                sqlcon.Open();
                string quary = "INSERT INTO News_update VALUES(@Title,@Des)";
                SqlCommand sqlcmd = new SqlCommand(quary, sqlcon);
                sqlcmd.Parameters.AddWithValue("@Title", newsLetter.Title);
                sqlcmd.Parameters.AddWithValue("@Des", newsLetter.Des);
                sqlcmd.ExecuteNonQuery();
            }
            DataTable datatable = new DataTable();
            using (SqlConnection sqlcon = new SqlConnection(connString))
            {
                sqlcon.Open();
                SqlDataAdapter sqlda = new SqlDataAdapter("SELECT email FROM News_subscriber", sqlcon);
                sqlda.Fill(datatable);
            }
            for (int i = 0; i < datatable.Rows.Count; i++)
            {
                MailMessage mail = new MailMessage();
                mail.To.Add(datatable.Rows[i][0].ToString());
                mail.From = new MailAddress("pankajparikh199704@gmail.com");
                mail.Subject = newsLetter.Title;
                string Body = newsLetter.Des;
                mail.Body = Body;
                mail.IsBodyHtml = true;
                SmtpClient smtp = new SmtpClient();
                smtp.Host = "smtp.gmail.com";
                smtp.Port = 587;
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = new System.Net.NetworkCredential("pankajparikh199704@gmail.com", "pankaj@123"); // Enter seders User name and password   
                smtp.EnableSsl = true;
                smtp.Send(mail);
            }
            return RedirectToAction("Create");
        }

        // GET: NewsLetterAdmin/Edit/5
        public ActionResult Edit(int id)
        {
            NewsLetterAdminModel nlm = new NewsLetterAdminModel();
            DataTable datatable = new DataTable();
            using (SqlConnection sqlcon = new SqlConnection(connString))
            {
                sqlcon.Open();
                SqlDataAdapter sqlda = new SqlDataAdapter("SELECT * FROM News_update where news_Id = @Id", sqlcon);
                sqlda.SelectCommand.Parameters.AddWithValue("@Id", id);
                sqlda.Fill(datatable);
            }
            if (datatable.Rows.Count == 1)
            {
                nlm.Id = Convert.ToInt32(datatable.Rows[0][0].ToString());
                nlm.Title = datatable.Rows[0][1].ToString();
                nlm.Des = datatable.Rows[0][2].ToString();
                return View(nlm);
            }
            else
                return RedirectToAction("NewsLetterIndex");
        }

        // POST: NewsLetterAdmin/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(NewsLetterAdminModel nlm)
        {
            using (SqlConnection sqlcon = new SqlConnection(connString))
            {
                sqlcon.Open();
                string quary = "Update News_update SET title=@Title , description=@Des WHERE news_Id=@Id ";
                SqlCommand sqlcmd = new SqlCommand(quary, sqlcon);
                sqlcmd.Parameters.AddWithValue("@Id", nlm.Id);
                sqlcmd.Parameters.AddWithValue("@Title", nlm.Title);
                sqlcmd.Parameters.AddWithValue("@Des", nlm.Des);
                sqlcmd.ExecuteNonQuery();
            }
            return RedirectToAction("NewsLetterIndex");

        }

        // GET: NewsLetterAdmin/Delete/5
        public ActionResult Delete(int id)
        {
            using (SqlConnection sqlcon = new SqlConnection(connString))
            {
                sqlcon.Open();
                string quary = "DELETE FROM News_update WHERE news_Id=@Id ";
                SqlCommand sqlcmd = new SqlCommand(quary, sqlcon);
                sqlcmd.Parameters.AddWithValue("@Id", id);
                sqlcmd.ExecuteNonQuery();
            }
            return RedirectToAction("NewsLetterIndex");
        }

        // POST: NewsLetterAdmin/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(NewsLetterIndex));
            }
            catch
            {
                return View();
            }
        }
    }
}