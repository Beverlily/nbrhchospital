﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;
using System.Net;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using NBRHC.Models;
using NBRHC.Models.ViewModels;
using NBRHC.Data;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System.Diagnostics;

namespace NBRHC.Controllers
{
    public class AlertController : Controller
    {
        //Variable to hold the db
        private NBRHCCMSContext db;
        private readonly IHostingEnvironment _env;
        private readonly UserManager<ApplicationUser> _userManager;
        //private async Task<ApplicationUser> GetCurrentUserAsync() => await _userManager.GetUserAsync(HttpContext.User);

        //Christine says I need this code to acccess my database context
        //when you have a method that's the same name as the class, it's called a CONSTRUCTOR FUNCTION
        public AlertController(NBRHCCMSContext context)
        {
            db = context;
            //_env = env;
            //_userManager = userManager;

        }
        //Methods:
        //Show
        //New
        //List

        //The reason why this code exists is because it points the request URL localhost:4431/Alert/Show
        //actually points to my Views/Alert/Show.cshtml
        public ActionResult Show(int id)
        {
            Debug.WriteLine("Alert ID: " + id);
            var alert = db.Alerts.Where(a => a.ID == id).FirstOrDefault();
            return View(alert);
        }

        //I need to make sure that the request URL  = localhost:4431/Alert/New
        //actually goes to my Views/Alert/New.cshtml
        public async Task<ActionResult> New()
        {
            //var alert = await GetCurrentUserAsync();

            //AlertEdit alertEditView = new AlertEdit();
            //alertEditView.emergencies = db.Emergencies.ToList();

            return View();
        }

        //I need to have localhost:4431/Alert/List
        //go to views and display Views/Alert/list.cshtml view
        public ActionResult List()
        {
            //Alert/List is asking for all the alerts
            //var query = "select * from alerts";
            var alerts = db.Alerts.ToList();
            return View(alerts);
        }

        public ActionResult Create(string formdescription, string formstartdate, string formenddate)
        {
            Debug.WriteLine("Description: " + formdescription);
            Debug.WriteLine("Start date: " + formstartdate);
            Debug.WriteLine("End date: " + formenddate);

            var query = "INSERT INTO alerts (description, startTime, endTime)";
            query += "VALUES (@desc, @start, @end)";

            SqlParameter[] sqlParams = new SqlParameter[3];
            sqlParams[0] = new SqlParameter("@desc", formdescription);
            sqlParams[1] = new SqlParameter("@start", formstartdate);
            sqlParams[2] = new SqlParameter("@end", formenddate);

            db.Database.ExecuteSqlCommand(query, sqlParams);

            return RedirectToAction("list");
        }

        public ActionResult Delete(int? id)
        {
            string query = "DELETE FROM alerts WHERE ID = @id";
            string query2 = "DELETE FROM emergencies WHERE ID = @id";

            SqlParameter param = new SqlParameter("@id", id);
            db.Database.ExecuteSqlCommand(query, param);
            db.Database.ExecuteSqlCommand(query2, param);
            return RedirectToAction("List");
        }
    }
}
