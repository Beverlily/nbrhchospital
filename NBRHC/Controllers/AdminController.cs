﻿/*Referenced Christines code*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;
using System.Net;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using NBRHC.Models;
using NBRHC.Data;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System.Diagnostics;

namespace NBRHC.Controllers
{
    public class AdminController : Controller
    {

        private readonly NBRHCCMSContext db;
        private readonly UserManager<ApplicationUser> _userManager;
  
        private async Task<ApplicationUser> GetCurrentUserAsync() => await _userManager.GetUserAsync(HttpContext.User);

        public AdminController(NBRHCCMSContext context, UserManager<ApplicationUser> usermanager)
        {
            //Admin controller needs the database context (for interacting with the database)
            //The usermanager (for getting the current logged in user)
            //allows you to manage application users 
            //lets you get things like id... i.e. var userId = user?.Id;

            db = context;
            _userManager = usermanager;
        }

        public int GetUserDetails(ApplicationUser user)
        {
            //UserState
            //0 => No user account 
            //1 => Has user account 
            //2 => Has user account and has an admin account 
            if (user == null) return 0;
            if (user.AdminID == null) return 1; //User has no admin account
            else
            {
                return 2; //user has admin account 
            }
        }

        public async Task<ActionResult> Index()
        {
            var user = await GetCurrentUserAsync();
            var userstate = GetUserDetails(user);
            ViewData["UserState"] = userstate;
            ViewData["UserAdminID"] = 0; //default to 0

            //if there is a user 
            if(userstate > 0)
            {
                //if user is an admin 
                if (userstate == 2)
                {
                    ViewData["UserAdminID"] = user.AdminID;

                }

                //User has a user account and/or admin account so they can see list of admins 
                return View(await db.Admins.ToListAsync());
            }
            else
            {
                //Public website visitors cannot access admin
                return RedirectToAction("Login", "Account");
            }
        }

        public ActionResult Show(int id)
        {
            return RedirectToAction("Details/" + id);
        }

        //Admin/Details/
        public async Task<ActionResult> Details(int? id)
        {
            //bad request
            if (id == null)
            {
                return new StatusCodeResult(400);
            }
        
            var user = await GetCurrentUserAsync();
            var userstate = GetUserDetails(user);
            ViewData["UserState"] = userstate;
            ViewData["UserAdminID"] = 0; //default
          
            if (userstate == 2)
            {
                ViewData["UserAdminID"] = user.AdminID;
            }

            //Check if the admin exists 
            Admin located_admin = await db.Admins.Include(a => a.Pages).SingleOrDefaultAsync(a => a.AdminID == id); 
            if (located_admin == null)
            {
                return NotFound();
            }

            return View(located_admin);
        }

        //GET: Admins/Create
        public async Task<ActionResult> Create()
        {
            var user = await GetCurrentUserAsync();

            //Users that have a user account can create an admin 
            if (user != null)
            {
                return View();
            }
            else
            {
                //Public website visitors can't create an admin 
                return Forbid();
            }
        }

        // POST: Admins/Create
        //Bind binds the properties () to the model 
        //Gets the values to populate the properties from the POST HTTP Request 
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind("AdminID,AdminFirstName,AdminLastName")] Admin admin)
        {
            //Model errors are stored in model state... if no errors (i.e. values are able to bind to the model)
            //then it is valid
            if (ModelState.IsValid)
            {
                db.Admins.Add(admin);
                db.SaveChanges();
                //Assigns the current logged in user to the current admin
                //Maps the current user's admin to this admin and makes the account the admin
                await MapUserToAdmin(admin);
            }
            return RedirectToAction("Index");
        }

        // GET: Admins/Edit/
        public async Task<ActionResult> Edit(int? id)
        {
            //bad request or no admin with that id 
            if (id == null || db.Admins.Find(id) == null)
            {
                return NotFound();
            }

            Admin admin = db.Admins.Find(id);
            var user = await GetCurrentUserAsync();
            var userstate = GetUserDetails(user);

            //if user has an admin account and is on the right account  
            if (userstate == 2 && id == user.AdminID)
            {
                return View(admin);
            }
            else
            {
                //wrong account or not an admin 
                return Forbid();
            }
        }

        //POST: Admins/Edit/
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind("AdminID, AdminFirstName, AdminLastName, UserID, User")] Admin admin)
        {

            var user = await GetCurrentUserAsync();
            var userstate = GetUserDetails(user);
            
            //user is admin 
            if (userstate == 2)
            {
                //wrong account 
                if (user.AdminID != admin.AdminID) return Forbid();
                if (ModelState.IsValid)
                {
                    db.Entry(admin).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                else
                {
                    //Values are not valid, displays error messages 
                    return View(admin);
                }
            }
            return NotFound();
        }

        // GET: Admin/Delete
        public async Task<ActionResult> Delete(int? id){
            //bad request
            if (id == null)
            {
                return new StatusCodeResult(400);
            }
            var admin = await db.Admins.FindAsync(id);
            if (admin == null)
            {
                return NotFound();
            }

            var user = await GetCurrentUserAsync();

            //No user account or wrong account 
            if (user == null || user.AdminID != id)
            {
                return Forbid();
            }

            return View(admin);
        }

        // POST: Admin/Delete
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Admin admin = await db.Admins.FindAsync(id);
            var user = await GetCurrentUserAsync();
            if (user.AdminID != id)
            {
                return Forbid();
            }
            //Deletes pages associated with admin 
            db.Pages.RemoveRange(db.Pages.Where(p => p.AdminID == id));
            await UnmapUserFromAdmin(id);
            db.Admins.Remove(admin);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        //Unlinks one to one relationship between user and admin 
        public async Task<IActionResult> UnmapUserFromAdmin(int id)
        {
            //Removes user from admin
            Admin admin = await db.Admins.FindAsync(id);
            admin.User = null;
            admin.UserID = "";

            if (ModelState.IsValid)
            {
                db.Entry(admin).State = EntityState.Modified;
                var admin_response = await db.SaveChangesAsync();

                //No changes detected
                if (admin_response == 0)
                {
                    return BadRequest(admin_response);
                }
                else
                {
                    //Unmap user from admin is successful
                    //Removes admin from user 
                    var user = await GetCurrentUserAsync();
                    user.Admin = null;
                    user.AdminID = null;
                    var user_response = await _userManager.UpdateAsync(user);
                    if (user_response == IdentityResult.Success)
                    {
                        Debug.WriteLine("I was able to update the user");
                        return Ok();
                    }
                    else
                    {
                        Debug.WriteLine("I was not able to update the user");
                        return BadRequest(user_response);
                    }
                }
            }
            else
            {
                return BadRequest("Unstable Model.");
            }

        }

        private async Task<IActionResult> MapUserToAdmin(Admin admin)
        {
            var user = await GetCurrentUserAsync();
            //mapping the user to the admin
            user.Admin = admin;
            var user_response = await _userManager.UpdateAsync(user);

            //checking confirmation of mapping user to admin
            if (user_response == IdentityResult.Success)
            {
                Debug.WriteLine("Admin has been mapped to the user.");
            }
            else
            {
                Debug.WriteLine("Admin has not been able to be mapped to the user.");
                return BadRequest(user_response);
            }

            //mapping the admin to the user
            admin.User = user;
            admin.UserID = user.Id;

            //checks if admin has been mapped to user 
            if (ModelState.IsValid)
            {
                db.Entry(admin).State = EntityState.Modified;
                var admin_response = await db.SaveChangesAsync();
                if (admin_response > 0) //some admins affected
                {
                    //user has been mapped to admin and admin has been mapped to user 
                    return Ok();
                }
                else
                {
                    return BadRequest(admin_response);
                }
            }
            else
            {
                return BadRequest("Unstable Admin Model.");
            }

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}
