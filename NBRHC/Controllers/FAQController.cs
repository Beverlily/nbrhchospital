﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;
using System.Net;
using System.Web;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using NBRHC.Models;
using NBRHC.Data;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System.Diagnostics;

namespace NBRHC.Controllers
{
    //Note: This controller references Christine Bittle's http5204-blog-aspcore example.
    //Comments as well as code may look similar for learning purposes but is applied to this specific feature
    public class FAQController : Controller
    {
        //Make a Db Context for CRUD
        private readonly NBRHCCMSContext db;
        //We need the usermanager class to get things like the id or name
        private readonly UserManager<ApplicationUser> _userManager;
        //This function will return the current user at some point when called
        private async Task<ApplicationUser> GetCurrentUserAsync() => await _userManager.GetUserAsync(HttpContext.User);

        public FAQController(NBRHCCMSContext context, UserManager<ApplicationUser> usermanager)
        {
            //When our FAQ controller is created, we need 2 things
            //Db Context for CRUD
            db = context;

            //User manager for getting the current logged in user
            _userManager = usermanager;
        }

        public async Task<int> GetUserDetails(ApplicationUser user)
        {
            //UserState
            //0 => Public user 
            //1 => User but has no admin account 
            //2 => User has admin account but hasn't created any FAQs 
            //3 => User has admin account and has created one or more FAQs 
            if (user == null) return 0;
            if (user.AdminID == null) return 1; //User has no admin account 
            else
            {
                //User has admin account
                var userFAQs = await db.FAQs.Where(f => f.AdminID == user.AdminID).ToListAsync();
                var userFAQCount = userFAQs.Count();
                if (userFAQCount == 0) return 2; 
                else if (userFAQCount > 0) return 3; 
            }
            return -1; //something went wrong
        }

        public async Task<bool> IsUserFAQOwner(ApplicationUser user, int FAQID)
        {
            //public user 
            if (user == null) return false;
            //User but no admin account 
            if (user.AdminID == null) return false;

            var userFAQs = await
                db
                .FAQs
                .Include(f => f.Admin)
                .Where(f => f.AdminID == user.AdminID)
                .Where(f => f.FAQID == FAQID)
                .ToListAsync();
            if (userFAQs.Count() > 0) return true; //user is the owner of the FAQ
            return false;
        }

        // GET: FAQs
        public async Task<ActionResult> Index(int pagenum)
        {
            //Get current user
            var user = await GetCurrentUserAsync();
            var userstate = await GetUserDetails(user);

            //Info on user
            ViewData["UserState"] = userstate;
            ViewData["UserAdminID"] = 0;

            if(userstate == 3)
            {
                ViewData["UserAdminID"] = user.AdminID;
            }

            //Pagination
            //Referenced from:
            //Christine Bittle's http5204-blog-aspcore example BlogController.cs line 135
            var _faqs = await db.FAQs.ToListAsync();
            int faqcount = _faqs.Count();
            int perpage = 5;
            int maxpage = (int)Math.Ceiling((decimal)faqcount / perpage) - 1;
            if (maxpage < 0) maxpage = 0;
            if (pagenum < 0) pagenum = 0;
            if (pagenum > maxpage) pagenum = maxpage;
            int start = perpage * pagenum;
            ViewData["pagenum"] = (int)pagenum;
            ViewData["PaginationSummary"] = "";
            if (maxpage > 0)
            {
                ViewData["PaginationSummary"] =
                    (pagenum + 1).ToString() + " of " +
                    (maxpage + 1).ToString();
            }

            //return View(await db.FAQs.Skip(start).Take(perpage).ToListAsync());
            return View(await db.FAQs.Skip(start).Take(perpage).Include(f => f.Admin).ToListAsync());
        }

        public ActionResult Show(int id)
        {
            //Redirects to Details
            return RedirectToAction("Details/" + id);
        }

        // GET: FAQ/Details
        public async Task<ActionResult> Details(int id)
        {
            //Get current user
            var user = await GetCurrentUserAsync();
            var userstate = await GetUserDetails(user);
            ViewData["UserState"] = userstate;

            //If user is not logged in
            if (userstate == 0)
            {
                return new StatusCodeResult(400);
            }

            if(userstate == 3)
            {
                ViewData["IsUserFAQOwner"] = await IsUserFAQOwner(user, id);
            }

            return View(await db.FAQs.Include(f => f.Admin).SingleOrDefaultAsync(f => f.FAQID == id));

            //string query = "select * from FAQs where FAQID=@id";
            //SqlParameter[] myParams = new SqlParameter[1];
            //myParams[0] = new SqlParameter("@id", id);

            //FAQ myFAQ = db.FAQs.FromSql(query, myParams).FirstOrDefault();
            //return View(myFAQ);
        }

        // GET: FAQs/Create
        public async Task<ActionResult> Create()
        {
            var user = await GetCurrentUserAsync();
            var userstate = await GetUserDetails(user);

            if(userstate == 0)
            {
                return RedirectToAction("Login", "Account");
            }
            else if(userstate == 1)
            {
                return RedirectToAction("Create", "Admin");
            }
            else
            {
                ViewData["UserAdminID"] = user.AdminID;
                ViewData["UserAdmin"] = user.Admin;
            }

            return View();
        }

        // POST: FAQs/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind("FAQID, FAQQuestion, FAQAnswer")] FAQ faq)
        {
            //If not errors in the ModelState
            if (ModelState.IsValid)
            {
                //Add row and save
                db.FAQs.Add(faq);
                db.SaveChanges();

                TempData["AddSuccess"] = "FAQ successfully added";

                //Map our admin to the FAQ
                var res = await MapAdminToFAQ(faq);
                return RedirectToAction("Index");
            }
            else
            {
                TempData["AddFail"] = "Failed to add FAQ";
                return RedirectToAction("Index");
            }
        }

        // GET: FAQs/Edit
        public async Task<ActionResult> Edit(int id)
        {
            var user = await GetCurrentUserAsync();
            var userstate = await GetUserDetails(user);

            //Find out FAQ we want to edit
            FAQ faq = db.FAQs.Find(id);
            
            if(faq == null)
            {
                return NotFound();
            }
            else
            {
            //If user does not exist or is not an admin then Forbid
            if(userstate == 0)
            {
                return Forbid();
            }
            if(userstate == 1)
            {
                return Forbid();
            }
            if (userstate == 3)
            {
                bool FAQOwner = await IsUserFAQOwner(user, id);
                if (FAQOwner) return View(faq);
            }


            return Forbid();

            }


        }

        // POST: FAQs/Edit
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind("FAQID, FAQQuestion, FAQAnswer, AdminID")] FAQ faq)
        {
            //If no errors in the model state
            if(ModelState.IsValid)
            {
                db.Entry(faq).State = EntityState.Modified;
                db.SaveChanges();
                TempData["EditSuccess"] = "FAQ successfully edited";
                return RedirectToAction("Details/" + faq.FAQID);
            }
            else
            {
                TempData["EditFail"] = "Failed to edit FAQ";
                return RedirectToAction("Details/" + faq.FAQID);
            }

        }

        // FAQs/Delete
        public async Task<ActionResult> Delete(int id)
        {
            //Find FAQ we want to delete
            FAQ faq = db.FAQs.Find(id);
            if(id == null || faq == null)
            {
                return NotFound();
            }
            else
            {
                //Get current user and only allow them to delete if they're an admin
                var user = await GetCurrentUserAsync();
                var userstate = await GetUserDetails(user);

                if (userstate == 0 || faq.AdminID != user.AdminID)
                {
                    return Forbid();
                } 
                else
                {
                    bool FAQOwner = await IsUserFAQOwner(user, id);
                    if(FAQOwner)
                    {
                    await UnmapAdminFromFAQ(id);
                    db.FAQs.Remove(faq);
                    db.SaveChanges();
                    TempData["DeleteSuccess"] = "FAQ successfully deleted";
                    return RedirectToAction("Index");
                    }
                }
                return Forbid();
            }
        }

        private async Task<IActionResult> MapAdminToFAQ(FAQ faq)
        {
            //Get current user
            var user = await GetCurrentUserAsync();
            var userstate = await GetUserDetails(user);
            
            //If user has an admin
            if (userstate == 2 || userstate == 3)
            {
                //Find the admin associated to user
                Admin admin = await db.Admins.FindAsync(user.AdminID);
                var user_response = await _userManager.UpdateAsync(user);

                //Confirm mapping
                if(user_response == IdentityResult.Success)
                {
                    Debug.WriteLine("Mapping admin to FAQ successful.");
                }
                else
                {
                    Debug.WriteLine("Mapping admin to FAQ unsuccessful.");
                    return BadRequest(user_response);
                }

                //Mapping FAQ to admin
                faq.Admin = admin;
                faq.AdminID = admin.AdminID;

                if(ModelState.IsValid)
                {
                    db.Entry(faq).State = EntityState.Modified;
                    var page_response = await db.SaveChangesAsync();

                    if(page_response > 0)
                    {
                        return Ok();
                    }
                    else
                    {
                        return BadRequest(page_response);
                    }
                }
                else
                {
                    return BadRequest("Unstable FAQ model");
                }
            }
            else
            {
                return BadRequest("Must be an admin to create a FAQ");
            }
            
        }
        public async Task<IActionResult> UnmapAdminFromFAQ(int id)
        {
            //stripping admin from faq
            FAQ faq = await db.FAQs.FindAsync(id);
            faq.Admin = null;
            faq.AdminID = null;
            if (ModelState.IsValid)
            {
                db.Entry(faq).State = EntityState.Modified;
                var faq_res = await db.SaveChangesAsync();
                if (faq_res == 0)//No changes detected
                {
                    return BadRequest(faq_res);
                }
                else
                {
                    return Ok();
                }
            }
            else
            {
                return BadRequest("Unstable Model.");
            }

        }

    }
}