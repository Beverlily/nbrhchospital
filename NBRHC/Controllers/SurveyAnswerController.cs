﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;
using System.Net;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using NBRHC.Models;
using NBRHC.Models.ViewModels;
using NBRHC.Data;
using System.Diagnostics;

namespace NBRHC.Controllers
{
    public class SurveyAnswerController : Controller
    {
        private readonly NBRHCCMSContext db;

        public SurveyAnswerController(NBRHCCMSContext context)
        {
            db = context;
        }
        public ActionResult New()
        {
            
            SurveyAnswerEdit employeeeditview = new SurveyAnswerEdit();
            employeeeditview.SurveyQuestions = db.SurveyQuestions.ToList();

            
            return View(employeeeditview);
        } 
        
        [HttpPost]
        public ActionResult New(string[] question_id)
        { 

            //string query = "insert into surveyquestions (Question) values (@question)";



            //SqlParameter myparams = new SqlParameter("@question", Question_New);


            //db.Database.ExecuteSqlCommand(query, myparams);
            //testing that the paramters do indeed pass to the method
            //Debug > Windows > Output
            //Debug.WriteLine(query);

            return RedirectToAction("List");
        }

    }
}