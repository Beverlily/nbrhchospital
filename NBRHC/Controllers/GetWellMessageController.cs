﻿//Referenced Christine's code
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;
using System.Net;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using NBRHC.Models;
using NBRHC.Data;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System.Diagnostics;

namespace NBRHC.Controllers
{
    public class GetWellMessageController : Controller
    {
        private readonly NBRHCCMSContext db;
        private readonly UserManager<ApplicationUser> _userManager;
        private async Task<ApplicationUser> GetCurrentUserAsync() => await _userManager.GetUserAsync(HttpContext.User);

        public GetWellMessageController(NBRHCCMSContext context, UserManager<ApplicationUser> usermanager)
        {
            db = context;
            _userManager = usermanager;
        }

        public int GetUserDetails(ApplicationUser user)
        {
            //UserState
            //0 => Public user 
            //1 => User but has no admin account 
            //2 => User has admin account 
            if (user == null) return 0;
            if (user.AdminID == null) return 1; //User has no admin account 
            else if (user.AdminID != null) return 2; //user has admin account
            else return -1; //something went wrong
        }

        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        public async Task<ActionResult> List(int pageNum)
        {
            var user = await GetCurrentUserAsync();
            var userstate = GetUserDetails(user);

            //information about the type of user 
            ViewData["UserState"] = userstate;
            
            //if there is a user
            if (userstate > 0)
            {
                /*Pagination - Referenced Christines code */
                var pages = await db.GetWellMessages.ToListAsync();
                int pageCount = pages.Count();
                int perpage = 10;
                int maxpage = (int)Math.Ceiling((decimal)pageCount / perpage) - 1;
                if (maxpage < 0) maxpage = 0;
                if (pageNum < 0) pageNum = 0;
                if (pageNum > maxpage) pageNum = maxpage;
                int start = perpage * pageNum;
                ViewData["pagenum"] = (int)pageNum;
                ViewData["PaginationSummary"] = "";
                if (maxpage > 0)
                {
                    ViewData["PaginationSummary"] =
                        (pageNum + 1).ToString() + " of " +
                        (maxpage + 1).ToString();
                }
                //logged in users can see list of get well messages 
                return View(await db.GetWellMessages.Skip(start).Take(perpage).ToListAsync());
            }
            else
            {
                //public website visitors cannot see list of get well messages
                return View();
            }    

        }

        //Get Well Message/Details/
        public async Task<ActionResult> Details(int? id)
        {
            //bad request or get well message with that id doesn't exist 
            if (id == null || db.GetWellMessages.Find(id) == null)
            {
                return NotFound();
            }

            GetWellMessage getWellMessage = db.GetWellMessages.Find(id);
            var user = await GetCurrentUserAsync();
            var userstate = GetUserDetails(user);
            ViewData["UserState"] = userstate;

            if (userstate > 0)
            {
                return View(getWellMessage);
            }
            else
            {
               //public web visitors can't see get well messages 
               return Forbid();
            }
        }

        // GET: GetWellMessage/New
        public async Task<ActionResult> New()
        {
            var user = await GetCurrentUserAsync();
            var userstate = GetUserDetails(user);
            //if there is a user 
            if (userstate > 0)
            {
                //Users and admin users cannot create get well messages 
                return Forbid();
            }
            else
            {
                //Website visitors can create get well messages 
                return View();
            }
        }

        // POST: GetWellMessage/New
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> New([Bind("GetWellMessageID, GetWellMessageSenderName, GetWellMessageSenderEmail, " +
            " GetWellMessageTitle, GetWellMessageContent, GetWellMessagePatientName, GetWellMessageAreaName," +
            " GetWellMessageRoomNumber")] GetWellMessage getWellMessage)
        {
            //Checks user
            var user = await GetCurrentUserAsync();
            var userstate = GetUserDetails(user);
            //if there is a user 
            if (userstate > 0)
            {
                //Users and admin users cannot create get well messages 
                return Forbid();
            }
            else
            {
                //Checks against validation attributes in model 
                if (ModelState.IsValid)
                {
                    //default values
                    getWellMessage.GetWellMessageDateSubmitted = DateTime.Now;
                    getWellMessage.GetWellMessageApprove = false;

                    db.GetWellMessages.Add(getWellMessage);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                else
                {
                    return View();
                }
            }
        }

        //GET: GetWellMessage/Edit
        public async Task<ActionResult> Edit(int? id)
        {
            //bad request or page with that id doesn't exist 
            if (id == null || db.GetWellMessages.Find(id) == null)
            {
                return NotFound();
            }
            else
            {
                GetWellMessage getWellMessage = db.GetWellMessages.Find(id);
                var user = await GetCurrentUserAsync();
                var userstate = GetUserDetails(user);

                //can't edit get well message unless they have an admin account
                if (userstate < 2)
                {
                    return Forbid(); 
                }
                else
                {
                    return View(getWellMessage);
                }
            }
        }

        //POST: GetWellMessage/Edit 
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind("GetWellMessageID, GetWellMessageSenderName, GetWellMessageSenderEmail, GetWellMessageDateSubmitted, " +
            "GetWellMessageTitle, GetWellMessageContent, GetWellMessageApprove, GetWellMessagePatientName, " +
            "GetWellMessageAreaName, GetWellMessageRoomNumber")] GetWellMessage getWellMessage)
        {
            //checks user 
            var user = await GetCurrentUserAsync();
            var userstate = GetUserDetails(user);

            //can't edit get well message unless they have an admin account
            if (userstate < 2)
            {
                return Forbid();
            }
            else
            {
                //if no errors (i.e. required fields are empty), then changes are saved 
                if (ModelState.IsValid)
                {
                    db.Entry(getWellMessage).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Details/" + getWellMessage.GetWellMessageID);
                }
                //If there are errors, stays on same page & displays errors 
                return View(getWellMessage);
            }
        }

        //GetWellMessage/Delete
        public async Task<ActionResult> Delete(int? id)
        {
            //bad request or get well message does not exist 
            if (id == null || db.GetWellMessages.Find(id) == null)
            {
                return NotFound();
            }
            else
            {
                var user = await GetCurrentUserAsync();
                var userstate = GetUserDetails(user);
                //can't delete get well message if user is not logged in an admin account 
                if (user == null || user.AdminID == null)
                {
                    return Forbid();
                }
                else
                {
                    db.GetWellMessages.Remove(db.GetWellMessages.Find(id));
                    db.SaveChanges();
                    return RedirectToAction("List");
                }
            }
        }
    }
}