﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;
using System.Net;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using NBRHC.Models;
using NBRHC.Models.ViewModels;
using NBRHC.Data;
using System.Diagnostics;

namespace NBRHC.Controllers
{ //reference from christine code
    public class DoctorController : Controller
    {
        private readonly NBRHCCMSContext db;
        private readonly UserManager<ApplicationUser> _userManager;
        private async Task<ApplicationUser> GetCurrentUserAsync() => await _userManager.GetUserAsync(HttpContext.User);

        public DoctorController(NBRHCCMSContext context, UserManager<ApplicationUser> usermanager)
        {
            db = context;
            _userManager = usermanager;
        }
        public ActionResult Index()
        {
            return RedirectToAction("List");
        }
        //Reference from Christine code
        public async Task<ActionResult> List(int pagenum)
        {
            var user = await GetCurrentUserAsync();

            var doctorslist = await db.Doctors.ToListAsync();
            int doctorcount = doctorslist.Count();
            int perpage = 1;
            int maxpage = (int)Math.Ceiling((decimal)doctorcount / perpage) - 1;
            if (maxpage < 0) maxpage = 0;
            if (pagenum < 0) pagenum = 0;
            if (pagenum > maxpage) pagenum = maxpage;
            int start = perpage * pagenum;
            ViewData["pagenum"] = (int)pagenum;
            ViewData["PaginationSummary"] = "";
            if (maxpage > 0)
            {
                ViewData["PaginationSummary"] =
                    (pagenum + 1).ToString() + " of " +
                    (maxpage + 1).ToString();
            }
            //if there is a user 
            if (user != null)
            {
                if (user.AdminID == null)
                {
                    //user doens't have admin account 
                    ViewData["UserHasAdmin"] = "False";
                }
                else
                {
                    //user has admin account 
                    ViewData["UserHasAdmin"] = user.AdminID.ToString();
                }
 
                List<Doctor> doctors = await db.Doctors.Include(d => d.Admin).Skip(start).Take(perpage).ToListAsync();
                return View(doctors);
            }
            else
            {
                //Public website visitors can  view all doctor information. 
                ViewData["UserHasAdmin"] = "None";

                List<Doctor> doctors = await db.Doctors.Skip(start).Take(perpage).ToListAsync();
                return View(doctors);
            }
        }

        //public ActionResult List()
        //{
         
        //    List<Doctor> doctors = db.Doctors.ToList();
        //    return View(doctors);
        //}

        public async Task<ActionResult> New()
        {
            var user = await GetCurrentUserAsync();
            //DoctorEdit doctoreditview = new DoctorEdit();
            if (user != null && user.AdminID != null)
            {
                ViewData["UserHasAdmin"] = user.AdminID;
                ViewData["UserAdmin"] = user.Admin;

                //GOTO Views/Doctor/New.cshtml
               // return View(doctoreditview);
                return View();
            }
            else
            {
                //Public website visitors and users who are not admins cannot create doctor 
                return Forbid();
            }


        }

        [HttpPost]
        public ActionResult New(string DoctorDep_New, string DoctorF_New, string DoctorL_New, string DoctorEmail_New, int DoctorPhn_New, string DoctorWH_New, int AdminID, Admin Admin)
        {
              
            string query = "insert into doctors (DoctorDepName,DoctorFName,DoctorLName,EmailID,PhoneNumber,WorkingHours, AdminID) values (@department,@firstname,@lastname,@email,@phone, @workinghours, @adminID)";

            SqlParameter[] myparams = new SqlParameter[8];
            //@department paramter
            myparams[0] = new SqlParameter("@department", DoctorDep_New);
            //@firstname parameter
            myparams[1] = new SqlParameter("@firstname", DoctorF_New);
            //@lastname paramter
            myparams[2] = new SqlParameter("@lastname", DoctorL_New);
            //@email parameter
            myparams[3] = new SqlParameter("@email", DoctorEmail_New);
            //@phone number parameter
            myparams[4] = new SqlParameter("@phone", DoctorPhn_New);
            //@workinghours parameter
            myparams[5] = new SqlParameter("@workinghours", DoctorWH_New);
            myparams[6] = new SqlParameter("@adminID", AdminID);
            

            db.Database.ExecuteSqlCommand(query, myparams); 
            return RedirectToAction("List");
        }

        public async Task<ActionResult> Edit(int? id)
        {
            //DoctorEdit doctoreditview = new DoctorEdit();
            //doctoreditview.Doctor = db.Doctors.SingleOrDefault(d => d.DoctorID == id);

            if (id == null || db.Doctors.Find(id) == null)
            {
                return NotFound();
            }

            Doctor doctor = db.Doctors.Find(id); 
            var user = await GetCurrentUserAsync();

            //No user account or user is not admin 
            if (user == null || user.AdminID == null)
            {
                return Forbid();
            }
            else
            {
                return View(doctor);
            }

            //GOTO: Views/Doctor/Edit.cshtml
            //return View(doctoreditview);
        }

        [HttpPost]
        public ActionResult Edit(int id, string DoctorDepName, string DoctorFName, string DoctorLName, string EmailID, int PhoneNumber, string WorkingHours)
        {

            //debug.writeline(); <-- try to print out every variable value
            //reason: to test that the interface is passing the information to the controller
            //and also to test that you are getting inside this function

            Debug.WriteLine("we know that the doctor department name is "+DoctorDepName);

            string query = "update doctors set DoctorDepName=@department, DoctorFName=@firstname, DoctorLName=@lastname,EmailID=@email,PhoneNumber=@phone,WorkingHours=@workinghours where doctorid=@id";

            SqlParameter[] myparams = new SqlParameter[7];
            //@department Parameter 
            myparams[0] = new SqlParameter("@department", DoctorDepName);
            //@first Parameter  
            myparams[1] = new SqlParameter("@firstname", DoctorFName);
            //@lastname Parameter 
            myparams[2] = new SqlParameter("@lastname", DoctorLName);
            //@email parameter
            myparams[3] = new SqlParameter("@email", EmailID);
            //@phone number parameter
            myparams[4] = new SqlParameter("@phone", PhoneNumber);
            //@workinghours parameter
            myparams[5] = new SqlParameter("@workinghours", WorkingHours);
            //Pararameter for (doctor) id PRIMARY KEY
            myparams[6] = new SqlParameter("@id", id);

            
            db.Database.ExecuteSqlCommand(query, myparams);
            Debug.WriteLine(query);

            //GOTO: View/Doctor/Show.cshtml 
            return RedirectToAction("Show/" + id);
        }

        public ActionResult Show(int? id)
        {
           
            if ((id == null) || (db.Doctors.Find(id) == null))
            {
                return NotFound();

            }
            
            string query = "select * from doctors where doctorid=@id";

            SqlParameter myparam = new SqlParameter("@id", id);
            Doctor doctor = db.Doctors.SingleOrDefault(d => d.DoctorID == id);

            return View(doctor);

        }
        public async Task<ActionResult> Delete(int? id)
        {
            if ((id == null) || (db.Doctors.Find(id) == null))
            {
                return NotFound();

            }
            else
            {
                Doctor doctor = db.Doctors.Find(id);
                var user = await GetCurrentUserAsync();

                //can't delete because user is not logged in or user is not admin
                if (user == null || user.AdminID == null)
                {
                    return Forbid();
                }
                else
                {
                    string query = "delete from Doctors where doctorid=@id";
                    SqlParameter param = new SqlParameter("@id", id);
                    db.Database.ExecuteSqlCommand(query, param);
                    return RedirectToAction("List");
                }

            }
            
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}