﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NBRHC.Models;

namespace NBRHC.Controllers
{
    public class SubscribeController : Controller
    {
        string connString = ConnString.connString;
        // GET: Subscribe
        public ActionResult Index()
        {
            return View();
        }

        // GET: Subscribe/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Subscribe/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Subscribe/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(SubscribeModel subscribeModel)
        {
            using (SqlConnection sqlcon = new SqlConnection(connString))
            {
                sqlcon.Open();
                string quary = "INSERT INTO News_subscriber VALUES(@name,@email)";
                SqlCommand sqlcmd = new SqlCommand(quary, sqlcon);
                sqlcmd.Parameters.AddWithValue("@name", subscribeModel.name);
                sqlcmd.Parameters.AddWithValue("@email", subscribeModel.email);
                sqlcmd.ExecuteNonQuery();
            }
            return RedirectToAction("Create");
        }

        // GET: Subscribe/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Subscribe/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Subscribe/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Subscribe/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}