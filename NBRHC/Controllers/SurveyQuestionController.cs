﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;
using System.Net;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using NBRHC.Models;
using NBRHC.Models.ViewModels;
using NBRHC.Data;
using System.Diagnostics;

namespace NBRHC.Controllers
{
    public class SurveyQuestionController : Controller
    {
        private readonly NBRHCCMSContext db;
        private readonly UserManager<ApplicationUser> _userManager;
        private async Task<ApplicationUser> GetCurrentUserAsync() => await _userManager.GetUserAsync(HttpContext.User);

        public SurveyQuestionController(NBRHCCMSContext context, UserManager<ApplicationUser> usermanager)
        {
            db = context;
            _userManager = usermanager;
        }

        public async Task<int> GetUserDetails(ApplicationUser user)
        {
            //check if the user is an author or not.
            //UserState
            //0 => User has admin account 
            var userID = user.Id;
            if (user.AdminID == null) return 0; //User has no admin account 
            else
            {
                //User has admin acconut 
                return 1;

            }
        }

        public ActionResult Index()
        {
            return RedirectToAction("List");
        }
        //Reference from Christine code 
        public async Task<ActionResult> List(int pagenum)
        {

            var questions = await db.SurveyQuestions.ToListAsync();
            int surveycount = questions.Count();
            int perpage = 4;
            int maxpage = (int)Math.Ceiling((decimal)surveycount / perpage) - 1;
            if (maxpage < 0) maxpage = 0;
            if (pagenum < 0) pagenum = 0;
            if (pagenum > maxpage) pagenum = maxpage;
            int start = perpage * pagenum;
            ViewData["pagenum"] = (int)pagenum;
            ViewData["PaginationSummary"] = "";
            if (maxpage > 0)
            {
                ViewData["PaginationSummary"] =
                    (pagenum + 1).ToString() + " of " +
                    (maxpage + 1).ToString();
            }

            List<SurveyQuestion> surveyquestions = await db.SurveyQuestions.Skip(start).Take(perpage).ToListAsync();
            return View(surveyquestions);
        }

        //public ActionResult List()
        //{
        //    List<SurveyQuestion> surveyquestions = db.SurveyQuestions.ToList();
        //    return View(surveyquestions);
        //}
        public ActionResult New()
        {
            
            SurveyQuestionEdit surveyquestioneditview = new SurveyQuestionEdit();

            
            return View(surveyquestioneditview);
        }
        [HttpPost]
        public ActionResult New(string Question_New)
        {

            string query = "insert into surveyquestions (Question) values (@question)";


            
            SqlParameter myparams = new SqlParameter("@question", Question_New);
            

            db.Database.ExecuteSqlCommand(query, myparams);
            //testing that the paramters do indeed pass to the method
            //Debug>Windows>Output
            Debug.WriteLine(query);

            return RedirectToAction("List");
        }
        public ActionResult Edit(int id)
        {

            SurveyQuestionEdit surveyquestioneditview = new SurveyQuestionEdit();
            surveyquestioneditview.SurveyQuestion = db.SurveyQuestions.SingleOrDefault(sq => sq.QuestionID == id);
            return View(surveyquestioneditview);
        }
        [HttpPost]
        public ActionResult Edit(int id, string Question)
        {


            string query = "update surveyquestions set Question=@question where questionid=@id";


            SqlParameter[] myparams = new SqlParameter[2];

            myparams[0] = new SqlParameter("@question", Question);

            myparams[1] = new SqlParameter("@id", id);


            db.Database.ExecuteSqlCommand(query, myparams);
            Debug.WriteLine(query);

            return RedirectToAction("Show/" + id);
        }
        public ActionResult Show(int? id)
        {
            if ((id == null) || (db.SurveyQuestions.Find(id) == null))
            {
                return NotFound();

            }

            string query = "select * from surveyquestions where questionid=@id";

            SqlParameter myparam = new SqlParameter("@id", id);

            SurveyQuestion surveyquestion = db.SurveyQuestions.SingleOrDefault(q => q.QuestionID == id);

            return View(surveyquestion);

        }
        public ActionResult Delete(int? id)
        {
            if ((id == null) || (db.SurveyQuestions.Find(id) == null))
            {
                return NotFound();

            }

            
            string query = "delete from SurveyQuestions where questionid=@id";
            SqlParameter param = new SqlParameter("@id", id);
            db.Database.ExecuteSqlCommand(query, param);
            return RedirectToAction("List");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}