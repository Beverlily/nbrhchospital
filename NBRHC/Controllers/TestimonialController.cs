﻿//Referenced Christine's code
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;
using System.Net;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using NBRHC.Models;
using NBRHC.Data;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System.Diagnostics;
namespace NBRHC.Controllers
{
    public class TestimonialController : Controller
    {
        private readonly NBRHCCMSContext db;
        private readonly IHostingEnvironment _env;
        private readonly UserManager<ApplicationUser> _userManager;
        private async Task<ApplicationUser> GetCurrentUserAsync() => await _userManager.GetUserAsync(HttpContext.User);

        public TestimonialController(NBRHCCMSContext context, IHostingEnvironment env, UserManager<ApplicationUser> usermanager)
        {
            db = context;
            _env = env;
            _userManager = usermanager;
        }

        public int GetUserDetails(ApplicationUser user)
        {
            //UserState
            //Referenced Christines code
            //0 => Public user 
            //1 => User but has no admin account 
            //2 => User has admin account 
            if (user == null) return 0;
            if (user.AdminID == null) return 1; //User has no admin account 
            else if (user.AdminID != null) return 2; //user has admin account
            else return -1; //something went wrong
        }

        public Testimonial CheckImage(Testimonial testimonial, IFormFile testimonialImg)
        {
            //Referenced Christines code
            if (testimonialImg?.Length > 0)
            {
                //file extensioncheck taken from https://www.c-sharpcorner.com/article/file-upload-extension-validation-in-asp-net-mvc-and-javascript/
                var validTypes = new[] { "jpeg", "jpg", "png", "gif" };
                var extension = Path.GetExtension(testimonialImg.FileName).Substring(1);
                var webRoot = _env.WebRootPath;
                if (validTypes.Contains(extension))
                {
                    string path = Path.Combine(webRoot, "images/testimonials");

                    if (testimonial.ImgFilename != null)
                    {
                        path = Path.Combine(path, testimonial.ImgFilename);
                        //Deletes previous image 
                        //Referenced https://stackoverflow.com/questions/37204268/how-to-delete-the-uploaded-files-in-the-app-data-uploads-folder-asp-net-mvc
                        if (System.IO.File.Exists(path))
                        {
                            System.IO.File.Delete(path);
                        }
                    }
                    //generic .img extension, web translates easily.
                    //Referenced: https://stackoverflow.com/questions/12457084/saving-uploaded-file-with-unique-name-in-folder
                    string filename = System.DateTime.Now.ToString("ddMMyyhhmmss") + "." + extension;

                    //get a direct file path to imgs/testimonials/
                    path = Path.Combine(webRoot, "images/testimonials");
                    path = Path.Combine(path, filename);

                    //save the file
                    using (var stream = new FileStream(path, FileMode.Create))
                    {
                        testimonialImg.CopyTo(stream);
                    }

                    //let the model know that there is a picture with an extension
                    testimonial.HasPic = 1;
                    testimonial.ImgFilename = filename;
                }
            }
            return testimonial;
        }

        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        //Testimonial/Details/
        public async Task<ActionResult> Details(int? id)
        {
            //bad request or page with that id doesn't exist 
            if (id == null || db.Testimonials.Find(id) == null)
            {
                return NotFound();
            }

            Testimonial testimonial = db.Testimonials.Find(id);
            var user = await GetCurrentUserAsync();
            var userstate = GetUserDetails(user);
            ViewData["UserState"] = userstate;

            //if user has a user account 
            if (userstate > 0)
            {
                return View(testimonial);
            }
            else
            {
                //Public website visitors cannot access pages that aren't published 
                if (testimonial.TestimonialPublish == true) return View(testimonial);
                else return Forbid();
            }
        }

        public async Task<ActionResult> List(int pageNum)
        {
            var user = await GetCurrentUserAsync();
            var userstate = GetUserDetails(user);

            //information about the type of user 
            ViewData["UserState"] = userstate;

            List<Testimonial> testimonials;
            int perpage;

            if (userstate > 0)
            {
                //if there is a user 
                //users and admin users can see list of testimonials 
                testimonials = await db.Testimonials.ToListAsync();
                perpage = 15;
            }
            else
            {
                //Public website visitors can only view published pages 
                testimonials = await db.Testimonials.Where(t => t.TestimonialPublish.Equals(true)).ToListAsync();
                perpage = 3;
            }

            /*Pagination - Referenced Christines code */
            int pageCount = testimonials.Count();
            int maxpage = (int)Math.Ceiling((decimal)pageCount / perpage) - 1;
            if (maxpage < 0) maxpage = 0;
            if (pageNum < 0) pageNum = 0;
            if (pageNum > maxpage) pageNum = maxpage;
            int start = perpage * pageNum;
            ViewData["pagenum"] = (int)pageNum;
            ViewData["PaginationSummary"] = "";
            if (maxpage > 0)
            {
                ViewData["PaginationSummary"] =
                    (pageNum + 1).ToString() + " of " +
                    (maxpage + 1).ToString();
            }

            return View(testimonials.Skip(start).Take(perpage).ToList());
        }

        // GET: Testimonial/New
        public async Task<ActionResult> New()
        {
            var user = await GetCurrentUserAsync();
            var userstate = GetUserDetails(user);
            ViewData["UserState"] = userstate;

            //Users and admin users cannot create testimonials
            if (userstate > 0)
            {
                return Forbid();
            }
            else
            {
            //Website visitors can write a testimonial
                return View();
            } 
        
        }

        // POST: Testimonial/New
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> New([Bind("TestimonialID, TestimonialName, TestimonialEmail, " +
            " TestimonialTitle, TestimonialStatement", "TestimonialPublishAgreement")] Testimonial testimonial, IFormFile testimonialImg)
        {
            var user = await GetCurrentUserAsync();
            var userstate = GetUserDetails(user);
      
            //if there is a user
            if (userstate > 0) return Forbid(); //Users and admin users cannot create testimonials
            else
            {
                //Website visitors can write a testimonial
                //By default testimonials do not have images
                testimonial.HasPic = 0;

                //Checks to see if public user uploaded a picture with their testimonial 
                testimonial = CheckImage(testimonial, testimonialImg);

                //Checks against validation attributes in model 
                if (ModelState.IsValid)
                {
                    //default values
                    testimonial.TestimonialDate = DateTime.Now;
                    testimonial.TestimonialApprove = false;
                    testimonial.TestimonialPublish = false;

                    db.Testimonials.Add(testimonial);
                    db.SaveChanges();
                    return RedirectToAction("List");
                }
                else
                {
                    return View();
                }
            }
        }

        public async Task<ActionResult> Edit(int? id)
        {
            //bad request or page with that id doesn't exist 
            if (id == null || db.Testimonials.Find(id) == null)
            {
                return NotFound();
            }
            else
            {
                Testimonial testimonial = db.Testimonials.Find(id);
                var user = await GetCurrentUserAsync();
                var userstate = GetUserDetails(user);

                //can't edit testimonial unless they have an admin account
                if (userstate < 2)
                {
                    return Forbid();
                }
                else
                {
                    return View(testimonial);
                }
            }
        }

        
        //POST: Testimonial/Edit 
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind("TestimonialID, TestimonialName, TestimonialDate, TestimonialEmail, TestimonialTitle, " +
            "TestimonialStatement, TestimonialApprove, TestimonialPublish, TestimonialPublishAgreement, HasPic, ImgFilename")] Testimonial testimonial, IFormFile testimonialImg)
        {
           //Checks user 
            var user = await GetCurrentUserAsync();
            var userstate = GetUserDetails(user);

            //Can't edit a testimonial unless you're an admin
            if (userstate < 2)
            {
                //user is a public visitor 
                return Forbid();
            }
            else
            {
                //user is an admin 

                //Checks to see if public user uploaded a picture with their testimonial 
                testimonial = CheckImage(testimonial, testimonialImg);

                //if no errors (i.e. required fields are empty), then changes are saved 
                if (ModelState.IsValid)
                {
                    db.Entry(testimonial).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Details/" + testimonial.TestimonialID);
                }
                //If there are errors, stays on same page & displays errors 
                return View(testimonial);
            }
        }

        public async Task<ActionResult> Delete(int? id)
        {
            //bad request or testimonial does not exist 
            if (id == null || db.Testimonials.Find(id) == null)
            {
                return NotFound();
            }
            else
            {
                var user = await GetCurrentUserAsync();
                var userstate = GetUserDetails(user);
                //can't delete testimonial if user is not logged in an admin account 
                if (userstate < 2)
                {
                    return Forbid();
                }
                else
                {
                    Testimonial testimonial = db.Testimonials.Find(id);

                    //If testimonial has image, deletes the image from testimonials image folder 
                    if (testimonial.HasPic == 1)
                    {
                        var webRoot = _env.WebRootPath;
                        string path = Path.Combine(webRoot, "images/testimonials");
                        path = Path.Combine(path, testimonial.ImgFilename);
                       //Referenced https://stackoverflow.com/questions/37204268/how-to-delete-the-uploaded-files-in-the-app-data-uploads-folder-asp-net-mvc
                        if (System.IO.File.Exists(path))
                        {
                            System.IO.File.Delete(path);
                        }
                    }

                    db.Testimonials.Remove(testimonial);
                    db.SaveChanges();
                    return RedirectToAction("List");
                }
            }
        }
    }
}