﻿// <auto-generated />
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.EntityFrameworkCore.Storage.Internal;
using NBRHC.Data;
using System;

namespace NBRHC.Migrations
{
    [DbContext(typeof(NBRHCCMSContext))]
    partial class NBRHCCMSContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.0.3-rtm-10026")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityRole", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("Name")
                        .HasMaxLength(256);

                    b.Property<string>("NormalizedName")
                        .HasMaxLength(256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedName")
                        .IsUnique()
                        .HasName("RoleNameIndex")
                        .HasFilter("[NormalizedName] IS NOT NULL");

                    b.ToTable("AspNetRoles");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityRoleClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("RoleId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetRoleClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("UserId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserLogin<string>", b =>
                {
                    b.Property<string>("LoginProvider");

                    b.Property<string>("ProviderKey");

                    b.Property<string>("ProviderDisplayName");

                    b.Property<string>("UserId")
                        .IsRequired();

                    b.HasKey("LoginProvider", "ProviderKey");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserLogins");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserRole<string>", b =>
                {
                    b.Property<string>("UserId");

                    b.Property<string>("RoleId");

                    b.HasKey("UserId", "RoleId");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetUserRoles");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserToken<string>", b =>
                {
                    b.Property<string>("UserId");

                    b.Property<string>("LoginProvider");

                    b.Property<string>("Name");

                    b.Property<string>("Value");

                    b.HasKey("UserId", "LoginProvider", "Name");

                    b.ToTable("AspNetUserTokens");
                });

            modelBuilder.Entity("NBRHC.Models.Admin", b =>
                {
                    b.Property<int>("AdminID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("AdminFirstName")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.Property<string>("AdminLastName")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.Property<string>("UserID");

                    b.HasKey("AdminID");

                    b.ToTable("Admins");
                });

            modelBuilder.Entity("NBRHC.Models.Alert", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("description")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.Property<string>("endTime")
                        .HasMaxLength(255);

                    b.Property<string>("startTime")
                        .HasMaxLength(255);

                    b.HasKey("ID");

                    b.ToTable("Alerts");
                });

            modelBuilder.Entity("NBRHC.Models.AlertCategory", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("category")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.HasKey("ID");

                    b.ToTable("AlertCategories");
                });

            modelBuilder.Entity("NBRHC.Models.ApplicationUser", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("AccessFailedCount");

                    b.Property<int?>("AdminID");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("Email")
                        .HasMaxLength(256);

                    b.Property<bool>("EmailConfirmed");

                    b.Property<bool>("LockoutEnabled");

                    b.Property<DateTimeOffset?>("LockoutEnd");

                    b.Property<string>("NormalizedEmail")
                        .HasMaxLength(256);

                    b.Property<string>("NormalizedUserName")
                        .HasMaxLength(256);

                    b.Property<string>("PasswordHash");

                    b.Property<string>("PhoneNumber");

                    b.Property<bool>("PhoneNumberConfirmed");

                    b.Property<string>("SecurityStamp");

                    b.Property<bool>("TwoFactorEnabled");

                    b.Property<string>("UserName")
                        .HasMaxLength(256);

                    b.HasKey("Id");

                    b.HasIndex("AdminID")
                        .IsUnique()
                        .HasFilter("[AdminID] IS NOT NULL");

                    b.HasIndex("NormalizedEmail")
                        .HasName("EmailIndex");

                    b.HasIndex("NormalizedUserName")
                        .IsUnique()
                        .HasName("UserNameIndex")
                        .HasFilter("[NormalizedUserName] IS NOT NULL");

                    b.ToTable("AspNetUsers");
                });

            modelBuilder.Entity("NBRHC.Models.ContactUs", b =>
                {
                    b.Property<int>("ContactID")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("ContactDateSubmitted")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValueSql("getDate()");

                    b.Property<string>("ContactEmail")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<string>("ContactMessage")
                        .IsRequired()
                        .HasMaxLength(2147483647);

                    b.Property<string>("ContactName")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<string>("ContactPhone")
                        .IsRequired()
                        .HasMaxLength(10);

                    b.Property<string>("ContactSubject")
                        .IsRequired()
                        .HasMaxLength(100);

                    b.HasKey("ContactID");

                    b.ToTable("ContactUs");
                });

            modelBuilder.Entity("NBRHC.Models.Doctor", b =>
                {
                    b.Property<int>("DoctorID")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("AdminID");

                    b.Property<string>("DoctorDepName")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.Property<string>("DoctorFName")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.Property<string>("DoctorLName")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.Property<string>("EmailID")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.Property<int>("PhoneNumber")
                        .HasMaxLength(255);

                    b.Property<string>("WorkingHours")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.HasKey("DoctorID");

                    b.HasIndex("AdminID");

                    b.ToTable("Doctors");
                });

            modelBuilder.Entity("NBRHC.Models.Donation", b =>
                {
                    b.Property<int>("DonationId")
                        .ValueGeneratedOnAdd();

                    b.Property<double>("Amount");

                    b.Property<string>("Date")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.Property<int>("DonationGoalID");

                    b.Property<int>("DonorId");

                    b.HasKey("DonationId");

                    b.HasIndex("DonationGoalID");

                    b.HasIndex("DonorId");

                    b.ToTable("Donations");
                });

            modelBuilder.Entity("NBRHC.Models.DonationGoal", b =>
                {
                    b.Property<int>("DonationGoalId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("EndDate")
                        .HasMaxLength(255);

                    b.Property<double>("Goal");

                    b.Property<double>("Progress");

                    b.Property<string>("StartDate")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.Property<bool>("WasMet");

                    b.HasKey("DonationGoalId");

                    b.ToTable("DonationGoals");
                });

            modelBuilder.Entity("NBRHC.Models.Donor", b =>
                {
                    b.Property<int>("DonorId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("address")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.Property<string>("cellPhoneNo")
                        .HasMaxLength(255);

                    b.Property<string>("firstName")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.Property<string>("homePhoneNo")
                        .HasMaxLength(255);

                    b.Property<string>("lastName")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.HasKey("DonorId");

                    b.ToTable("Donors");
                });

            modelBuilder.Entity("NBRHC.Models.Emergency", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("category")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.Property<string>("description")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.Property<string>("endTime")
                        .HasMaxLength(255);

                    b.Property<string>("startTime")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.HasKey("ID");

                    b.ToTable("Emergencies");
                });

            modelBuilder.Entity("NBRHC.Models.Employee", b =>
                {
                    b.Property<int>("EmployeeID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("City")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.Property<string>("EmailID")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.Property<string>("EmployeeFName")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.Property<string>("EmployeeLName")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.Property<int>("JobID");

                    b.Property<int>("PhoneNumber")
                        .HasMaxLength(255);

                    b.Property<string>("Postalcode")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.Property<string>("StreetName")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.HasKey("EmployeeID");

                    b.HasIndex("JobID");

                    b.ToTable("Employees");
                });

            modelBuilder.Entity("NBRHC.Models.FAQ", b =>
                {
                    b.Property<int>("FAQID")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("AdminID");

                    b.Property<string>("FAQAnswer")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.Property<string>("FAQQuestion")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.HasKey("FAQID");

                    b.HasIndex("AdminID");

                    b.ToTable("FAQs");
                });

            modelBuilder.Entity("NBRHC.Models.FoodService", b =>
                {
                    b.Property<int>("FoodServiceID")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("AdminID");

                    b.Property<string>("FoodName")
                        .IsRequired()
                        .HasMaxLength(40);

                    b.Property<string>("FoodPrice")
                        .IsRequired()
                        .HasMaxLength(10);

                    b.Property<string>("PatientType")
                        .IsRequired()
                        .HasMaxLength(40);

                    b.HasKey("FoodServiceID");

                    b.HasIndex("AdminID");

                    b.ToTable("FoodServices");
                });

            modelBuilder.Entity("NBRHC.Models.Gallery", b =>
                {
                    b.Property<int>("ImageID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ImageAlt")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<string>("ImageName")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<string>("ImageType")
                        .HasMaxLength(50);

                    b.HasKey("ImageID");

                    b.ToTable("Gallery");
                });

            modelBuilder.Entity("NBRHC.Models.GetWellMessage", b =>
                {
                    b.Property<int>("GetWellMessageID")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("GetWellMessageApprove");

                    b.Property<string>("GetWellMessageAreaName")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.Property<string>("GetWellMessageContent")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.Property<DateTime>("GetWellMessageDateSubmitted");

                    b.Property<string>("GetWellMessagePatientName")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.Property<string>("GetWellMessageRoomNumber")
                        .IsRequired()
                        .HasMaxLength(100);

                    b.Property<string>("GetWellMessageSenderEmail")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.Property<string>("GetWellMessageSenderName")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.Property<string>("GetWellMessageTitle")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.HasKey("GetWellMessageID");

                    b.ToTable("GetWellMessages");
                });

            modelBuilder.Entity("NBRHC.Models.Job", b =>
                {
                    b.Property<int>("JobID")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("AdminID");

                    b.Property<string>("Department")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.Property<string>("JobDescription")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.Property<string>("JobTitle")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.HasKey("JobID");

                    b.HasIndex("AdminID");

                    b.ToTable("Jobs");
                });

            modelBuilder.Entity("NBRHC.Models.Page", b =>
                {
                    b.Property<int>("PageID")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("AdminID");

                    b.Property<string>("PageContent")
                        .HasMaxLength(2147483647);

                    b.Property<bool>("PagePublish");

                    b.Property<string>("PageTitle")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.HasKey("PageID");

                    b.HasIndex("AdminID");

                    b.ToTable("Pages");
                });

            modelBuilder.Entity("NBRHC.Models.Survey", b =>
                {
                    b.Property<int>("SurveyID")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("DateTaken")
                        .HasMaxLength(255);

                    b.HasKey("SurveyID");

                    b.ToTable("Surveys");
                });

            modelBuilder.Entity("NBRHC.Models.SurveyAnswer", b =>
                {
                    b.Property<int>("SurveyID");

                    b.Property<int>("QuestionID");

                    b.Property<string>("Answer")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.Property<int>("AnswerID");

                    b.HasKey("SurveyID", "QuestionID");

                    b.HasAlternateKey("AnswerID");

                    b.HasIndex("QuestionID");

                    b.ToTable("SurveyAnswer");
                });

            modelBuilder.Entity("NBRHC.Models.SurveyQuestion", b =>
                {
                    b.Property<int>("QuestionID")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("AdminID");

                    b.Property<string>("Question")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.HasKey("QuestionID");

                    b.HasIndex("AdminID");

                    b.ToTable("SurveyQuestions");
                });

            modelBuilder.Entity("NBRHC.Models.Testimonial", b =>
                {
                    b.Property<int>("TestimonialID")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("HasPic");

                    b.Property<string>("ImgFilename");

                    b.Property<bool>("TestimonialApprove");

                    b.Property<DateTime>("TestimonialDate");

                    b.Property<string>("TestimonialEmail")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.Property<string>("TestimonialName")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.Property<bool>("TestimonialPublish");

                    b.Property<bool>("TestimonialPublishAgreement");

                    b.Property<string>("TestimonialStatement")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.Property<string>("TestimonialTitle")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.HasKey("TestimonialID");

                    b.ToTable("Testimonials");
                });

            modelBuilder.Entity("NBRHC.Models.Volunteer", b =>
                {
                    b.Property<int>("id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("cellPhoneNo")
                        .HasMaxLength(255);

                    b.Property<string>("city")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.Property<string>("email")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.Property<string>("firstName")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.Property<string>("homePhoneNo")
                        .HasMaxLength(255);

                    b.Property<string>("lastName")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.Property<string>("streetNumber")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.HasKey("id");

                    b.ToTable("Volunteers");
                });

            modelBuilder.Entity("NBRHC.Models.VolunteerApplicant", b =>
                {
                    b.Property<int>("id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("cellPhoneNo")
                        .HasMaxLength(255);

                    b.Property<string>("city")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.Property<string>("email")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.Property<string>("firstName")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.Property<string>("homePhoneNo")
                        .HasMaxLength(255);

                    b.Property<string>("lastName")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.Property<string>("streetNumber")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.HasKey("id");

                    b.ToTable("VolunteerApplicants");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityRoleClaim<string>", b =>
                {
                    b.HasOne("Microsoft.AspNetCore.Identity.IdentityRole")
                        .WithMany()
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserClaim<string>", b =>
                {
                    b.HasOne("NBRHC.Models.ApplicationUser")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserLogin<string>", b =>
                {
                    b.HasOne("NBRHC.Models.ApplicationUser")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserRole<string>", b =>
                {
                    b.HasOne("Microsoft.AspNetCore.Identity.IdentityRole")
                        .WithMany()
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("NBRHC.Models.ApplicationUser")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserToken<string>", b =>
                {
                    b.HasOne("NBRHC.Models.ApplicationUser")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("NBRHC.Models.ApplicationUser", b =>
                {
                    b.HasOne("NBRHC.Models.Admin", "Admin")
                        .WithOne("User")
                        .HasForeignKey("NBRHC.Models.ApplicationUser", "AdminID");
                });

            modelBuilder.Entity("NBRHC.Models.Doctor", b =>
                {
                    b.HasOne("NBRHC.Models.Admin", "Admin")
                        .WithMany("Doctors")
                        .HasForeignKey("AdminID");
                });

            modelBuilder.Entity("NBRHC.Models.Donation", b =>
                {
                    b.HasOne("NBRHC.Models.DonationGoal", "DonationGoal")
                        .WithMany("Donations")
                        .HasForeignKey("DonationGoalID")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("NBRHC.Models.Donor", "Donor")
                        .WithMany("Donations")
                        .HasForeignKey("DonorId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("NBRHC.Models.Employee", b =>
                {
                    b.HasOne("NBRHC.Models.Job", "Job")
                        .WithMany("Employees")
                        .HasForeignKey("JobID")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("NBRHC.Models.FAQ", b =>
                {
                    b.HasOne("NBRHC.Models.Admin", "Admin")
                        .WithMany("FAQs")
                        .HasForeignKey("AdminID");
                });

            modelBuilder.Entity("NBRHC.Models.FoodService", b =>
                {
                    b.HasOne("NBRHC.Models.Admin", "Admin")
                        .WithMany("FoodServices")
                        .HasForeignKey("AdminID")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("NBRHC.Models.Job", b =>
                {
                    b.HasOne("NBRHC.Models.Admin", "Admin")
                        .WithMany("Jobs")
                        .HasForeignKey("AdminID");
                });

            modelBuilder.Entity("NBRHC.Models.Page", b =>
                {
                    b.HasOne("NBRHC.Models.Admin", "Admin")
                        .WithMany("Pages")
                        .HasForeignKey("AdminID");
                });

            modelBuilder.Entity("NBRHC.Models.SurveyAnswer", b =>
                {
                    b.HasOne("NBRHC.Models.SurveyQuestion", "SurveyQuestion")
                        .WithMany("SurveyAnswers")
                        .HasForeignKey("QuestionID")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("NBRHC.Models.Survey", "Survey")
                        .WithMany("SurveyAnswers")
                        .HasForeignKey("SurveyID")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("NBRHC.Models.SurveyQuestion", b =>
                {
                    b.HasOne("NBRHC.Models.Admin", "Admin")
                        .WithMany("SurveyQuestions")
                        .HasForeignKey("AdminID");
                });
#pragma warning restore 612, 618
        }
    }
}
