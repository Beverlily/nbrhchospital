﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System.Security.Claims;
using System.Threading.Tasks;
using NBRHC.Models;

//Defines database and tables
namespace NBRHC.Data
{
    public class NBRHCCMSContext : IdentityDbContext<ApplicationUser>
    {

        public NBRHCCMSContext(DbContextOptions<NBRHCCMSContext> options)
        : base(options)
        {

        }

        //Represents tables in DB
        public DbSet<Admin> Admins { get; set; }
        public DbSet<FAQ> FAQs { get; set; }
        public DbSet<ContactUs> ContactUs { get; set; }
        public DbSet<Gallery> Gallery { get; set; }
        public DbSet<Page> Pages { get; set; }
        public DbSet<Testimonial> Testimonials { get; set; }
        public DbSet<GetWellMessage> GetWellMessages { get; set; }
        public DbSet<Job> Jobs { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Doctor> Doctors { get; set; }
        public DbSet<Survey> Surveys { get; set; }
        public DbSet<SurveyQuestion> SurveyQuestions { get; set; }
        public DbSet<SurveyAnswer> SurveyAnswers { get; set; }
        

        /*ALEX*/
        
        public DbSet<Volunteer> Volunteers { get; set; }
        public DbSet<VolunteerApplicant> VolunteerApplicants { get; set; }
        public DbSet<Donation> Donations { get; set; }
        public DbSet<DonationGoal> DonationGoals { get; set; }
        public DbSet<Donor> Donors { get; set; }
        public DbSet<Alert> Alerts { get; set; }
        public DbSet<Emergency> Emergencies { get; set; }
        public DbSet<AlertCategory> AlertCategories { get; set; }

        /*PARAM*/
        public DbSet<FoodService> FoodServices { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            //Expression format: 
            //(input-parameters) => expression
            //expression lambda returns the result of the expression

            //One admin has one user and that user has one admin
            modelBuilder.Entity<Admin>()
                .HasOne(a => a.User)
                .WithOne(u => u.Admin)
                .HasForeignKey<ApplicationUser>(u => u.AdminID);

            //One page has one admin, one admin has many pages 
            modelBuilder.Entity<Page>()
             .HasOne(p => p.Admin)
             .WithMany(a => a.Pages)
             .HasForeignKey(p => p.AdminID);

            //One FAQ has one admin, one admin has many FAQs
            modelBuilder.Entity<FAQ>()
                .HasOne(f => f.Admin)
                .WithMany(a => a.FAQs)
                .HasForeignKey(f => f.AdminID);

            //Contact message default value for date submitted is the current date
            //https://docs.microsoft.com/en-us/ef/core/modeling/relational/default-values
            modelBuilder.Entity<ContactUs>()
                .Property(c => c.ContactDateSubmitted)
                .HasDefaultValueSql("getDate()");

            modelBuilder.Entity<SurveyAnswer>()
                  .HasKey(surans => new { surans.SurveyID, surans.QuestionID });

            //describing that SurveyAnswer is associated with one Survey

            modelBuilder.Entity<SurveyAnswer>()
                .HasOne(surans => surans.Survey)
                .WithMany(surans => surans.SurveyAnswers)
                .HasForeignKey(surans => surans.SurveyID);

            modelBuilder.Entity<SurveyAnswer>()
                .HasOne(surans => surans.SurveyQuestion)
                .WithMany(surans => surans.SurveyAnswers)
                .HasForeignKey(surans => surans.QuestionID);


            //Job has many employeess
            modelBuilder.Entity<Employee>()
                .HasOne(j => j.Job)
                .WithMany(j => j.Employees)
                .HasForeignKey(b => b.JobID);

            //Donation to Donor
            modelBuilder.Entity<Donation>()
                .HasOne(d => d.Donor)
                .WithMany(d => d.Donations)
                .HasForeignKey(d => d.DonorId);

            //Donation to DonationGoal
            modelBuilder.Entity<Donation>()
                .HasOne(d => d.DonationGoal)
                .WithMany(d => d.Donations)
                .HasForeignKey(d => d.DonationGoalID);

            //Param
            modelBuilder.Entity<FoodService>()
                .HasOne(f => f.Admin)
                .WithMany(a => a.FoodServices)
                .HasForeignKey(f => f.AdminID);


            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Admin>().ToTable("Admins");
            modelBuilder.Entity<Page>().ToTable("Pages");
            modelBuilder.Entity<FAQ>().ToTable("FAQs");
            modelBuilder.Entity<ContactUs>().ToTable("ContactUs");
            modelBuilder.Entity<Gallery>().ToTable("Gallery");
            modelBuilder.Entity<Testimonial>().ToTable("Testimonials");
            modelBuilder.Entity<GetWellMessage>().ToTable("GetWellMessages");
            modelBuilder.Entity<Job>().ToTable("Jobs");
            modelBuilder.Entity<Employee>().ToTable("Employees");
            modelBuilder.Entity<Doctor>().ToTable("Doctors");
            modelBuilder.Entity<Survey>().ToTable("Surveys");
            modelBuilder.Entity<SurveyQuestion>().ToTable("SurveyQuestions");
            modelBuilder.Entity<SurveyAnswer>().ToTable("SurveyAnswer");
            
            
            modelBuilder.Entity<Volunteer>().ToTable("Volunteers");
            modelBuilder.Entity<VolunteerApplicant>().ToTable("VolunteerApplicants");
            modelBuilder.Entity<Donation>().ToTable("Donations");
            modelBuilder.Entity<DonationGoal>().ToTable("DonationGoals");
            modelBuilder.Entity<Donor>().ToTable("Donors");
            modelBuilder.Entity<Alert>().ToTable("Alerts");
            modelBuilder.Entity<Emergency>().ToTable("Emergencies");
            modelBuilder.Entity<AlertCategory>().ToTable("AlertCategories");

            //paramm
            modelBuilder.Entity<FoodService>().ToTable("FoodServices");

        }
    }
}

