﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NBRHC.Models
{
    public class GetWellMessage
    {
        //ModelState.IsValid (in controller) checks the validation rules set by the validation attributes
        //Validation attributes like: StringLength & Required
        //DateTime is inherently required (must have value) so doesnt require Required
        //https://docs.microsoft.com/en-us/aspnet/core/tutorials/first-mvc-app/validation?view=aspnetcore-2.2

        [Key]
        public int GetWellMessageID { get; set; }

        [Required, StringLength(255), Display(Name = "Sender Name")]
        public string GetWellMessageSenderName { get; set; }

        [Required, StringLength(255), Display(Name = "Sender Email")]
        public string GetWellMessageSenderEmail { get; set; }

        //DataType does not provide validation, it is just a formatting attribute
        //that helps view engine format data
        [DataType(DataType.Date), Display(Name = "Date Submitted")]
        public DateTime GetWellMessageDateSubmitted { get; set; }

        [Required, StringLength(255), Display(Name = "Title")]
        public string GetWellMessageTitle { get; set; }

        [Required, DataType(DataType.MultilineText), StringLength(255), Display(Name = "Message")]
        public string GetWellMessageContent { get; set; }

        [Required, Display(Name = "Approve")] 
        public bool GetWellMessageApprove { get; set; }

        [Required, StringLength(255), Display(Name = "Patient Name")]
        public string GetWellMessagePatientName{ get; set; }

        [Required, StringLength(255), Display(Name = "Area Name")]
        public string GetWellMessageAreaName { get; set; }

        [Required, StringLength(100), Display(Name = "Room Number")]
        public string GetWellMessageRoomNumber { get; set; }

    }
}
