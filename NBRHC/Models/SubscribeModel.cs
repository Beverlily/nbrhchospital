﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace NBRHC.Models
{
    public class SubscribeModel
    {
        public int Id { get; set; }
        [Display(Name = "Name")]
        public string name { get; set; }
        [Display(Name = "Email")]
        [EmailAddress]
        public string email { get; set; }

    }
}
