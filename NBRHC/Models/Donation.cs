﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NBRHC.Models
{
    public class Donation
    {
        [Key]
        public int DonationId { get; set; }

        //Each donation has an amount
        [Required, Display(Name = "Amount")]
        public double Amount { get; set; }

        
        //Each donation has a date
        [Required, StringLength(255), Display(Name = "Date")]
        public string Date { get; set; }
 
        [ForeignKey("DonationGoalId")]
        public int DonationGoalID { get; set; }

        public virtual DonationGoal DonationGoal { get; set; }

        //Each donation was made by a donor
        [ForeignKey("DonorId")]
        public int DonorId { get; set; }
        
        public virtual Donor Donor { get; set; }
    }
}