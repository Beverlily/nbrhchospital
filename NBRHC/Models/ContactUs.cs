﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NBRHC.Models
{
    public class ContactUs
    {
        [Key]
        public int ContactID { get; set; }

        [Required, StringLength(50), Display(Name = "Name")]
        public string ContactName { get; set; }

        [Required, StringLength(50), Display(Name = "Email")]
        public string ContactEmail { get; set; }

        [Required, StringLength(10), Display(Name = "Phone")]
        public string ContactPhone { get; set; }

        [Required, StringLength(100), Display(Name = "Subject")]
        public string ContactSubject { get; set; }

        //DataType does not provide validation, it is just a formatting attribute
        //that helps view engine format data
        //Referenced from Beverly's GetWellMessage.cs
        [DataType(DataType.Date), Display(Name = "Date Submitted")]
        public DateTime ContactDateSubmitted { get; set; }

        [Required, StringLength(int.MaxValue), DataType(DataType.MultilineText), Display(Name = "Content")]
        public string ContactMessage { get; set; }

    }
}
