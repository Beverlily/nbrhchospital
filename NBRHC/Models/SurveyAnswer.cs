﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NBRHC.Models
{
    public class SurveyAnswer
    {
        [Key]
        public int AnswerID { get; set; }

        [Required, StringLength(255), Display(Name = "Answer")]
        public string Answer { get; set; }

        [ForeignKey("QuestionID")]
        public int QuestionID { get; set; }

        public virtual SurveyQuestion SurveyQuestion { get; set; }

        [ForeignKey("SurveyID")]
        public int SurveyID { get; set; }

        public virtual Survey Survey { get; set; }
    }
}
