﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NBRHC.Models
{
    public class DonationGoal
    {
        [Key]
        public int DonationGoalId { get; set; }

        //Describes the amount of money for the donation goal.
        [Required, Display(Name = "Donation goal")]
        public double Goal { get; set; }

        //Each donation goal has a start date.
        [Required, StringLength(255), Display(Name = "Start date")]
        public string StartDate { get; set; }

        //Each donation goal has an end date but it might not have been reached yet.
        [StringLength(255), Display(Name = "End date")]
        public string EndDate { get; set; }

        //Double number between 0 and 100. Represents the progress in %.
        [Required, Display(Name = "Progress")]
        public double Progress { get; set; }

        //Has the goal been met?
        [Required, Display(Name = "Was met")]
        public Boolean WasMet { get; set; }
        
        
        //All of the donations that were made for this donation goal
        [InverseProperty("DonationGoal")]
        public List<Donation> Donations { get; set; }
        
        
    }
}
