﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NBRHC.Models
{
    public class Job
    {
        [Key]
        public int JobID { get; set; }

        [Required, StringLength(255), Display(Name = "Job Title")]
        public string JobTitle { get; set; }

        [Required, StringLength(255), Display(Name = "Job Description")]
        public string JobDescription { get; set; }

        [Required, StringLength(255), Display(Name = "Department")]
        public string Department { get; set; }

        [InverseProperty("Job")]
        public virtual List<Employee> Employees { get; set; }

        [ForeignKey("AdminID")]
        public int? AdminID { get; set; }

        public virtual Admin Admin { get; set; }
    }
}
