﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace NBRHC.Models
{
    public class SurveyQuestion
    {
        [Key]
        public int QuestionID { get; set; }

        [Required, StringLength(255), Display(Name = "Question")]
        public string Question { get; set; }

        [InverseProperty("SurveyQuestion")]
        public virtual List<SurveyAnswer> SurveyAnswers { get; set; }

        [ForeignKey("AdminID")]
        public int? AdminID { get; set; }

        public virtual Admin Admin { get; set; }
    }
}
