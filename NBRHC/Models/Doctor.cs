﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NBRHC.Models
{
    public class Doctor
    {
        [Key]
        public int DoctorID { get; set; }

        [Required, StringLength(255), Display(Name = "Department Name")]
        public string DoctorDepName { get; set; }

        [Required, StringLength(255), Display(Name = "First Name")]
        public string DoctorFName { get; set; }

        [Required, StringLength(255), Display(Name = "Last Name")]
        public string DoctorLName { get; set; }

        [Required, StringLength(255), Display(Name = "Email")]
        public string EmailID { get; set; }

        [Required, StringLength(255), Display(Name = "Phone Number")]
        public int PhoneNumber { get; set; }

        [Required, StringLength(255), Display(Name = "Hours")]
        public string WorkingHours { get; set; }

        [ForeignKey("AdminID")]
        public int? AdminID { get; set; }

        public virtual Admin Admin { get; set; }
    }
}
