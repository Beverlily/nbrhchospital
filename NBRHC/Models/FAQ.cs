﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NBRHC.Models
{
    public class FAQ
    {
        [Key]
        public int FAQID { get; set; }

        [Required, StringLength(255), Display(Name = "Question")]
        public string FAQQuestion { get; set; }

        [Required, StringLength(255), Display(Name = "Answer")]
        public string FAQAnswer { get; set; }

        //One FAQ has one admin associated with it 
        [ForeignKey("AdminID")]
        public int? AdminID { get; set; }

        public virtual Admin Admin { get; set; }
    }
}
