﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NBRHC.Models
{
  public class FoodService
   {
        
       //param foodservice feature
        [Key, ScaffoldColumn(false)]
        public int FoodServiceID { get; set; }

        [Required, StringLength(40), Display(Name = "Food Name")]
        public string FoodName { get; set; }

        [Required, StringLength(40), Display(Name = "Specially For")]
        public string PatientType { get; set; }

        [Required, StringLength(10), Display(Name = "Food Price")]
        public string FoodPrice { get; set; }

  // admin functionality added.    
 [ForeignKey("AdminID")]
 public int AdminID { get; set; }
public virtual Admin Admin { get; set; }

    }
}
