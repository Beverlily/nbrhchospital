﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NBRHC.Models
{
    public class Survey
    {
        [Key]
        public int SurveyID { get; set; }

        [Required, StringLength(255), Display(Name = "Date")]
        public DateTime DateTaken { get; set; }

        [InverseProperty("Survey")]
        public virtual List<SurveyAnswer> SurveyAnswers { get; set; }
    }
}
