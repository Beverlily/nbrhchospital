﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NBRHC.Models
{
    public class Page
    {
        [Key]
        public int PageID { get; set; }

        [Required, StringLength(255), Display(Name = "Title")]
        public string PageTitle { get; set; }

        [StringLength(int.MaxValue), DataType(DataType.MultilineText), Display(Name = "Content")]
        public string PageContent { get; set; }

        [Required, Display(Name = "Publish")]
        public bool PagePublish { get; set; }

        //One page has one admin 
        [ForeignKey("AdminID")]
        public int? AdminID { get; set; }
     
        public virtual Admin Admin { get; set; }

    }
}
