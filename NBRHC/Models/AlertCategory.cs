﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NBRHC.Models
{
    public class AlertCategory
    {
        [Key]
        public int ID { get; set; }

        [Required, StringLength(255), Display(Name = "Category")]
        public string category { get; set; }
        /*
        [InverseProperty("AlertCategory")]
        public List<Alert> alerts { get; set; }
        */
    }
}
