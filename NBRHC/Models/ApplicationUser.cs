﻿/*Referenced Christines code*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NBRHC.Models
{
    public class ApplicationUser : IdentityUser
    {
        //One to one relationship between admin and user 
        [ForeignKey("AdminID")]
        public int? AdminID { get; set; } 

        public virtual Admin Admin { get; set; }
    }
}
