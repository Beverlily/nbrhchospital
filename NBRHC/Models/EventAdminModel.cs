﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NBRHC.Models
{
    public class EventAdminModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string stime { get; set; }
        public string etime { get; set; }
        public string sdate { get; set; }
        public string edate { get; set; }
        public string Location { get; set; }
        public string Image { get; set; }

        public IFormFile FileToUpload { get; set; }
    }
}
