﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NBRHC.Models.ViewModels
{
    public class DonationAdd
    {
        //Empty constructor
        public DonationAdd()
        {

        }

        public virtual Donation donation { get; set; }

        public IEnumerable<Donor> donors { get; set; }

        public IEnumerable<DonationGoal> donationgoals { get; set; }
    }
}
