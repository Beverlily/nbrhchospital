﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NBRHC.Models.ViewModels
{
    public class DonationEdit
    {
        public DonationEdit()
        {
            
        }
        
        public virtual Donation Donation { get; set; }

        public virtual Donor donor { get; set; }

        public IEnumerable<Donor> Donors { get; set; }

        public virtual DonationGoal donationgoal { get; set; }

        public IEnumerable<DonationGoal> DonationGoals { get; set; }

    }
}
