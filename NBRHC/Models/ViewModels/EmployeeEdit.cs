﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NBRHC.Models.ViewModels
{
    public class EmployeeEdit
    {
        public EmployeeEdit()
        {

        }

        

        public virtual Employee Employee { get; set; }

        public IEnumerable<Job> Jobs { get; set; }
    }
}
