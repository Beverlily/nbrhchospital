﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NBRHC.Models.ViewModels
{
    public class VolunteerApplicantEdit
    {
        public VolunteerApplicantEdit()
        {

        }

        public virtual VolunteerApplicant volunteerApplicant { get; set; }
    }
}
