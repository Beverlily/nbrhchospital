﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NBRHC.Models.ViewModels
{
    public class AlertEdit
    {
        public AlertEdit()
        {
            
        }

        public virtual Emergency emergency { get; set; }

        public IEnumerable<Emergency> emergencies { get; set; }
    }
}
