﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace NBRHC.Models
{
    public class Testimonial
    {
        [Key]
        public int TestimonialID { get; set; }

        [Required, StringLength(255), Display(Name = "Name")]
        public string TestimonialName { get; set; }

        [Required, StringLength(255), Display(Name = "Email")]
        public string TestimonialEmail { get; set; }

        [DataType(DataType.Date), Display(Name = "Date")]
        public DateTime TestimonialDate { get; set; }

        [Required, StringLength(255), Display(Name = "Title")]
        public string TestimonialTitle { get; set; }

        [Required, StringLength(255), Display(Name = "Statement")]
        public string TestimonialStatement { get; set; }

        [Required, Display(Name = "Approve")]
        public bool TestimonialApprove { get; set; }

        [Required, Display(Name = "Publish")]
        public bool TestimonialPublish { get; set; }

        [Range(typeof(bool), "true", "true", ErrorMessage = "Must be checked"), Display(Name = "I agree to have this published")]
        public bool TestimonialPublishAgreement { get; set; }

        //Referenced Christines MVC example
        //Picture
        //1 => Picture exists (in imgs/makeupproducts/{id}.ImgType)
        //0 => Picture doesn't exist
        public int HasPic { get; set; }

        //Accepted image formats (jpg/jpeg/png/gif)
        public string ImgFilename { get; set; }

    }
}
