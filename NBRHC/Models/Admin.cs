﻿/*Referenced Christines code*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NBRHC.Models
{
    public class Admin
    {
        [Key]
        public int AdminID { get; set; }

        [Required, StringLength(255), Display(Name = "First Name")]
        public string AdminFirstName { get; set; }

        [Required, StringLength(255), Display(Name = "Last Name")]
        public string AdminLastName { get; set; }

        //One to one relationship between admin and user
        [ForeignKey("UserID")]
        public string UserID { get; set; }

        public virtual ApplicationUser User { get; set; }

        //One admin has many pages 
        [InverseProperty("Admin")]
        public List<Page> Pages { get; set; }

        //One admin has many FAQs
        [InverseProperty("Admin")]
        public List<FAQ> FAQs { get; set; }

        [InverseProperty("Admin")]
        public List<Job> Jobs { get; set; }

        [InverseProperty("Admin")]
        public List<Doctor> Doctors { get; set; }

        [InverseProperty("Admin")]
        public List<SurveyQuestion> SurveyQuestions { get; set; }

        //param 
        [InverseProperty("Admin")]
        public List<FoodService> FoodServices { get; set; }

    }
}
