﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace NBRHC.Models
{
    public class Gallery
    {
        [Key]
        public int ImageID { get; set; }

        [Required, StringLength(50), Display(Name = "Alt")]
        public string ImageAlt { get; set; }

        [Required, StringLength(50), Display(Name = "Name")]
        public string ImageName { get; set; }

        [StringLength(50)]
        public string ImageType { get; set; }

    }
}
