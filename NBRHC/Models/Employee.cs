﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NBRHC.Models
{
    public class Employee
    {
        [Key]
        public int EmployeeID { get; set; }

        [Required, StringLength(255), Display(Name = "First Name")]
        public string EmployeeFName { get; set; }

        [Required, StringLength(255), Display(Name = "Last Name")]
        public string EmployeeLName { get; set; }

        [Required, StringLength(255), Display(Name = "Email")]
        public string EmailID { get; set; }

        [Required, StringLength(255), Display(Name = "Phone Number")]
        public int PhoneNumber { get; set; }

        [Required, StringLength(255), Display(Name = "Street Name")]
        public string StreetName { get; set; }

        [Required, StringLength(255), Display(Name = "City")]
        public string City { get; set; }

        [Required, StringLength(255), Display(Name = "Postalcode")]
        public string Postalcode { get; set; }

        [ForeignKey("JobID")]
        public int JobID { get; set; }

        public virtual Job Job { get; set; }
    }
}
